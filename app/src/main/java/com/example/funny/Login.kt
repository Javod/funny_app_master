package com.example.funny

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import com.example.funny.com.proyecto.credenciales.Credenciales

class Login : AppCompatActivity() {


    private lateinit var txtCorreo : EditText
    private lateinit var txtPass : EditText
    private lateinit var txtRegistrarse : TextView
    private lateinit var btnIngresar : Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        initComponents()



        //Evento declick para validar el ingreso del usuario
        btnIngresar.setOnClickListener(View.OnClickListener {


            if(txtCorreo.text.isEmpty())
            {
                mensaje("Ingrese su correo por favor")
            }
            else if( txtPass.text.isEmpty())
            {
                mensaje("Ingrese su contraseña por favor")
            }
            else
            {
                if(verifricarIngreso())
                {
                    startActivity(Intent(this, MainActivity::class.java))
                    finish()
                }
                else{
                    mensaje("Sus datos de ingreso no son correctos")
                }
            }


        })

        //Evento de click para dirigir al usuario al registro
        txtRegistrarse.setOnClickListener(View.OnClickListener {
            startActivity(Intent(this, Registrarse::class.java))
            finish()
        })
    }


    //funcion para validar datos de ingreso del usuario
    private fun verifricarIngreso() : Boolean{
        val credenciales = Credenciales(this)

        return credenciales.existUsuario()
    }

    //funcionn para inicializar componentes
    private fun initComponents()
    {
        txtCorreo = findViewById(R.id.txt_correo_login)
        txtPass= findViewById(R.id.txt_pass_login)
        txtRegistrarse = findViewById(R.id.txt_ingresa)
        btnIngresar= findViewById(R.id.btn_ingresar)
    }


    //funcion para escribir mensajes en pantalla
    private fun mensaje(msj : String)
    {
        Toast.makeText(this, msj,Toast.LENGTH_SHORT).show()
    }


}
