package com.example.funny


import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.widget.Button
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import com.example.funny.com.proyecto.beans.Club
import com.example.funny.com.proyecto.beans.Evento

import com.example.funny.com.proyecto.vistas.fragmentos.FragmentoListaEventos
import com.example.funny.com.proyecto.vistas.fragmentos.FragmentoListaLocales
import com.google.android.material.bottomappbar.BottomAppBar
import com.google.android.material.floatingactionbutton.FloatingActionButton


class MainActivity : AppCompatActivity(), FragmentoListaLocales.OnFragmentInteractionListener, FragmentoListaEventos.OnFragmentInteractionListener{


    lateinit  var bottonAppBar : BottomAppBar
    var fragmento  = FragmentoListaLocales()
    var fragmentoListaEventos = FragmentoListaEventos()
    var btnDistritos : FloatingActionButton? = null
    var dialogDistritos : AlertDialog.Builder? = null
    var btnDialogDistritos : Button? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)



        setTitle("EVENTOS")

        bottonAppBar = findViewById(R.id.botton_appbar)
        btnDistritos = findViewById(R.id.btn_buscar_distritos)

        //mostrarDialogDistritos()

        bottonAppBar.replaceMenu(R.menu.menu_botton)

        if(savedInstanceState== null)
        {
            mostarListaEventos()
        }

        //mostrarListaLocales()

        bottonAppBar.setOnMenuItemClickListener(Toolbar.OnMenuItemClickListener { item: MenuItem? ->when(item!!.itemId){
            R.id.menu->
            {
                //mostarDistrito(R.layout.botton_sheet_distritos)
                startActivity(Intent(this,UserActivity::class.java))
                return@OnMenuItemClickListener true
            }
            /*R.id.karaoke_menu->{
                mensaje("Encuentra Karaokes!!!")
                return@OnMenuItemClickListener true
            }*/
            R.id.discos_menu->{
                mostrarListaLocales()
                setTitle("CLUBS")

                mensaje("Encuentra Discotecas!!!")
                return@OnMenuItemClickListener true
            }
            /*
            R.id.box_menu->{
                mensaje("Encuentra Boxs Privados!!!")
                return@OnMenuItemClickListener true
            }*/
            R.id.evento_menu->{
                mensaje("Encuentra Eventos!!!")
                setTitle("EVENTOS")
                mostarListaEventos()
                return@OnMenuItemClickListener true
            }

            else -> {
                false
            }
        }  })

        btnDistritos?.setOnClickListener(View.OnClickListener { mostrarDialogDistritos() })
        btnDialogDistritos?.setOnClickListener(View.OnClickListener { mensaje("Hola!!!!") })
    }

    //Funcion para mostrar de Dialog  con los distritos
    fun mostrarDialogDistritos()
    {
        dialogDistritos = AlertDialog.Builder(this)

        val inflater = layoutInflater.inflate(R.layout.botton_sheet_distritos,null)
        dialogDistritos?.setView(inflater)
        dialogDistritos?.setPositiveButton("Listo"){dialog, which ->dialog.dismiss()}
        dialogDistritos?.show()
    }



    //Funcion pata mostar el fragmentos con los locales
    fun mostrarListaLocales()
    {
        val manager = supportFragmentManager
        val transaction =  manager.beginTransaction()
        transaction.replace(R.id.frgmento_principal,fragmento)
        transaction.addToBackStack(null)
        transaction.commit()
    }

    //Funcion para mostarel fragmentos con los eventos
    fun mostarListaEventos()
    {
        val manager = supportFragmentManager
        val transaction =  manager.beginTransaction()
        transaction.replace(R.id.frgmento_principal,fragmentoListaEventos)
        transaction.commit()
    }

    //Funcion para mostar mesajes por patalla
    fun mensaje(msj :String)
    {
        Toast.makeText(this,msj,Toast.LENGTH_SHORT).show()
    }

    //Funciones de las interfaces para comunicar
    //a los fragmentos con la actividad


    override fun mandarDatosEventos(e: Evento) {
        mensaje(e.nombreEvento)
        var bundle : Bundle =Bundle()
        val intent = Intent(applicationContext, LocalActivity::class.java)

        bundle.putSerializable("evento",e)

        intent.putExtras(bundle)


        startActivity(intent)

    }

    override fun enviarLocal(local: Club) {
        mensaje(local.nombreClub)
        var bundle : Bundle =Bundle()
        val intent = Intent(applicationContext, LocalActivity::class.java)

        bundle.putSerializable("local",local)

        intent.putExtras(bundle)


        startActivity(intent)
    }

    override fun onFragmentInteraction(uri: Uri) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }


}
