package com.example.funny

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.*
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.example.funny.com.proyecto.beans.Evento
import com.example.funny.com.proyecto.beans.Usuario
import com.example.funny.com.proyecto.beans.com.proyecto.beans.listas.IntegranteLista
import com.example.funny.com.proyecto.beans.com.proyecto.beans.listas.Lista
import com.example.funny.com.proyecto.com.proyecto.adaptadores.ModeloIntegranteLista
import com.example.funny.com.proyecto.com.proyecto.adaptadores.ModeloListaUser
import com.example.funny.com.proyecto.credenciales.Credenciales
import com.example.funny.com.proyecto.dialogos.mensajes.DialogAgregadoLista
import com.example.funny.com.proyecto.dialogos.mensajes.DialogBuscarLista
import com.example.funny.com.proyecto.dialogos.mensajes.DialogCodigoNoEncontrado
import com.example.funny.com.proyecto.dialogos.mensajes.DialogEstoyEnLista
import com.example.funny.com.proyecto.enums.PuntosFinales
import com.example.funny.com.proyecto.retrofitconnetion.ServiceEndPoint
import com.example.funny.com.viewmodel.IntegranteListaViewmodel
import com.example.funny.com.viewmodel.ListaViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_user.*
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

class UserActivity : AppCompatActivity(), DialogBuscarLista.InteractionDialogBuscarCodigoLista,
        DialogAgregadoLista.InteractionDialogAgregadoLista, DialogCodigoNoEncontrado.InteractionDialogCodigoNoEncontrado, DialogEstoyEnLista.CerrarDialogEstoyEnLista
{

    private lateinit var txtNombre: TextView
    private lateinit var txtCorreo : TextView
    private lateinit var irEditarDatos : ImageButton
    private lateinit var txtbuscarLista : ImageButton

    private lateinit var txtMisListas : ListView
    private lateinit var estoyEnLista : ListView

    var dialogoBuscarCodigo : DialogBuscarLista? = null

    var dialogAceptarAgredado : DialogAgregadoLista? = null

    var dialogCodigoNoEncontrado : DialogCodigoNoEncontrado? = null

    var dialogEstoyEnLista : DialogEstoyEnLista? = null

    private lateinit var   listaViewModel: ListaViewModel

    lateinit var  lista : Lista
    lateinit var integrante: IntegranteLista
    lateinit var eventosByLista : List<Evento>

    private var myCompositeDisposable: CompositeDisposable? = null

    private var myCompositeDisposable2: CompositeDisposable? = null

    private var myCompositeDisposable3: CompositeDisposable? = null

    private var myCompositeDisposable4: CompositeDisposable? = null

    lateinit var integranteViewmodel : IntegranteListaViewmodel

    lateinit var user : Usuario

    lateinit var numListas : TextView
    lateinit var numInvitados : TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_user)

        integranteViewmodel = run{ViewModelProviders.of(this).get(IntegranteListaViewmodel::class.java)}

        listaViewModel = run{ViewModelProviders.of(this).get(ListaViewModel::class.java)}

        initComponents()
        mostarDatosDeUsuario()

        irEditarDatos.setOnClickListener{irEditarDatosUsuario()}

        getEventosByIntegrante(user.idUsuario)
        observerLista()

        txtbuscarLista.setOnClickListener { mostarDialogBuscarLista() }

    }

    private fun initComponents()
    {
        txtNombre = findViewById(R.id.txt_nombre)
        txtCorreo = findViewById(R.id.txt_correo)
        txtbuscarLista = findViewById(R.id.txt_buscar_lista)
        txtMisListas = findViewById(R.id.txt_mis_listas)
        estoyEnLista = findViewById(R.id.estoy_en_lista)
        irEditarDatos = findViewById(R.id.ir_editar_datos)
        myCompositeDisposable = CompositeDisposable()
        myCompositeDisposable2= CompositeDisposable()
        myCompositeDisposable3= CompositeDisposable()
        myCompositeDisposable4= CompositeDisposable()
        numListas= findViewById(R.id.num_lista)
        numInvitados = findViewById(R.id.num_invitados)
    }


    fun observerLista()
    {
        val observer = Observer<List<Lista>>{items ->
            if(items.size!=0)
            {
                mensaje_no_hay_listas.visibility= View.GONE
                var listaOrdenada = items.sortedWith(compareByDescending({it.idLista}))
                val adapter = ModeloListaUser(listaOrdenada,applicationContext)
                txtMisListas.adapter=adapter
                txtMisListas.setOnItemClickListener { adapterView, view, i, l ->enviarLista(listaOrdenada.get(i))  }
                numListas.text="${items.size}"
            }
            else
            {
                numListas.text="0"
            }
        }
        listaViewModel.getAllListas.observe(this,observer)
    }

    fun mensaje(msj : String)
    {
        Toast.makeText(this,msj, Toast.LENGTH_SHORT).show()
    }

    fun getEventosByIntegrante(idUsuario : Int)
    {

        val retrofit = Retrofit.Builder().baseUrl(PuntosFinales.urlRaiz).addConverterFactory(
            GsonConverterFactory.create()).addCallAdapterFactory(RxJava2CallAdapterFactory.create()).build();

        val restCliente = retrofit.create(ServiceEndPoint::class.java)

        myCompositeDisposable2!!.add(restCliente.eventosByIntegrante(idUsuario)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe(this::asignarEventosLista))
    }

    fun asignarEventosLista(eventos : List<Evento>)
    {
        if(eventos.size!=0)
        {
            mensaje_no_estoy_en_listas.visibility=View.GONE
            this.eventosByLista = eventos
            numInvitados.text="${eventos.size}"
            val adapter = ModeloIntegranteLista(eventosByLista,this)
            estoyEnLista.adapter=adapter
            estoyEnLista.setOnItemClickListener{parent,view,position,id->
                mostrarDialogesEstoyEnLista(eventosByLista.get(position))
            }
        }
        else
        {
            numInvitados.text="0"
        }
    }


    fun getListaRest(codLista : String)
    {
        val retrofit = Retrofit.Builder().baseUrl(PuntosFinales.urlRaiz).addConverterFactory(
            GsonConverterFactory.create()).addCallAdapterFactory(RxJava2CallAdapterFactory.create()).build();

        val restCliente = retrofit.create(ServiceEndPoint::class.java)

        myCompositeDisposable!!.add(restCliente.listaByCodLista(codLista)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe({l->asignarLista(l)},{e->errorBuscandoLista(e)}))
    }

    fun asignarLista(lista: Lista)
    {
        this.lista= lista
        var credenciales = Credenciales(this)
        if(lista!=null)
        {
            dialogoBuscarCodigo!!.dismiss()
            mostrarDialogAgregadoLista("Te Agregaste a la lista: ${lista.nombreLista}")
            integrante = IntegranteLista(credenciales.getDataUserId(),lista.idLista,credenciales.getDataUserId(),true)
            postIntegranteLista(integrante)
        }
    }

    fun errorBuscandoLista(e : Throwable)
    {
        when(e)
        {
           e-> {
               dialogoBuscarCodigo!!.dismiss()
               mostrarDialogCodigoNoEncontrado(e.toString())
           }
        }
    }

    fun postIntegranteLista(integrante : IntegranteLista)
    {
        val retrofit = Retrofit.Builder().baseUrl(PuntosFinales.urlRaiz).addConverterFactory(
            GsonConverterFactory.create()).addCallAdapterFactory(RxJava2CallAdapterFactory.create()).build();

        val restCliente = retrofit.create(ServiceEndPoint::class.java)

        myCompositeDisposable2!!.add(restCliente.postIntegrante(integrante)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe(this::asignarIntegrante))

    }

    fun asignarIntegrante(integrante: IntegranteLista)
    {
        this.integrante = integrante
        integranteViewmodel.save(integrante)
        getEventosByIntegrante(user.idUsuario)
    }


    fun irEditarDatosUsuario()
    {
        startActivity(Intent(this,EditarDatosUsuario::class.java))
    }


    fun mostarDialogBuscarLista()
    {
        val manager = supportFragmentManager!!
        dialogoBuscarCodigo = DialogBuscarLista(this)
        dialogoBuscarCodigo!!.show(manager,"dialogo_buscar_codigo")
    }

    fun mostrarDialogAgregadoLista(nombreLista : String)
    {
        val manager =  supportFragmentManager
        dialogAceptarAgredado = DialogAgregadoLista(nombreLista, this)
        dialogAceptarAgredado!!.show(manager,"dialogo_aceptar_agregado")
    }

    fun mostrarDialogCodigoNoEncontrado(msj : String)
    {
        val manager = supportFragmentManager
        dialogCodigoNoEncontrado = DialogCodigoNoEncontrado(this)
        dialogCodigoNoEncontrado!!.show(manager,"dialog_codigo_no_encontrado")
    }

    fun mostrarDialogesEstoyEnLista(evento : Evento)
    {
        val manager = supportFragmentManager
        dialogEstoyEnLista = DialogEstoyEnLista(evento,this)
        dialogEstoyEnLista!!.show(manager,"dialog_estoy_en_lista")
    }

    private fun mostarDatosDeUsuario()
    {
        var credenciales = Credenciales(this)
        user = credenciales.getDataUser()!!
        txtNombre.text=user?.nombre
        txtCorreo.text=user?.correo
    }

    private fun enviarLista(lista : Lista)
    {
        val bundle : Bundle = Bundle()
        var intent = Intent(applicationContext,MisIntegrantesEnLista::class.java)
        bundle.putSerializable("lista",lista)
        intent.putExtras(bundle)
        startActivity(intent)
    }
    // interfaz para poder recibir el codigo del dialogo
    override fun cerrarDialogBuscarCodigoLista(codigo: String) {
        getListaRest(codigo)
    }
    // Interfaz para cerrar el dialogo confirmado lista
    override fun cerrarDialogAgregadoLista(dato: String) {
        dialogAceptarAgredado!!.dismiss()
    }
    // Interfas para cerrar el men saje de codigo no encontrado
    override fun cerraDialogCodigoNoEncontrado() {
        dialogCodigoNoEncontrado!!.dismiss()
    }

    //Método de interfaz para cerrar dialogo mostar estoy en lista
    override fun cerrarDialogEstoyEnLista() {
        dialogEstoyEnLista!!.dismiss()
    }

}
