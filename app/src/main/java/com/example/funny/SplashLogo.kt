package com.example.funny

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.view.animation.AnimationUtils
import android.widget.ImageView
import com.example.funny.com.proyecto.credenciales.Credenciales

class SplashLogo : AppCompatActivity() {




    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash_logo)


        var imgLogo = findViewById<ImageView>(R.id.img_logo_splash)

        imgLogo.animation= AnimationUtils.loadAnimation(this,R.anim.fade_animation_logo)
        Handler().postDelayed({

            if(evaluarCredenciales())
            {
                startActivity(Intent(this,MainActivity::class.java))
                finish()
            }
            else
            {
                getdataLocalidades()
                startActivity(Intent(this, Login::class.java))
                finish()
            }

        },3000)
    }

    private fun getdataLocalidades() {

    }

    fun evaluarCredenciales() : Boolean
    {
        val credenciales= Credenciales(this)

        val evaluar = credenciales.existUsuario()


        return evaluar
    }
}
