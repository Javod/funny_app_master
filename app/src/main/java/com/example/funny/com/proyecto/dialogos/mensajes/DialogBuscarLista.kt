package com.example.funny.com.proyecto.dialogos.mensajes

import android.app.AlertDialog
import android.app.Dialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.ProgressBar
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.FragmentManager
import com.example.funny.R

class DialogBuscarLista( val listener : InteractionDialogBuscarCodigoLista) : DialogFragment()
{
    var txtBuscarCodigo : EditText?=null
    var btnBuscarCodigo : Button?= null
    var progresBuscandoCodigo  : ProgressBar?=null

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        var view = activity!!.layoutInflater.inflate(R.layout.dialog_mostrar_mensaje, null)

        initcomponents(view)


        btnBuscarCodigo!!.setOnClickListener(View.OnClickListener {
            if(txtBuscarCodigo!!.text!=null)
            {
                progresBuscandoCodigo!!.visibility=View.VISIBLE
                it.isEnabled=false
                listener!!.cerrarDialogBuscarCodigoLista(txtBuscarCodigo!!.text.toString())

            }
        })

        val dialog = AlertDialog.Builder(activity)
        dialog.setView(view)

        return dialog.create()
    }


    fun initcomponents(view : View)
    {
        txtBuscarCodigo = view.findViewById(R.id.txt_buscar_codigo)
        btnBuscarCodigo = view.findViewById(R.id.btn_buscar_codigo)
        progresBuscandoCodigo = view.findViewById(R.id.progres_buscando_codigo)
    }

    interface InteractionDialogBuscarCodigoLista
    {
        fun cerrarDialogBuscarCodigoLista(codigo : String)
    }
}