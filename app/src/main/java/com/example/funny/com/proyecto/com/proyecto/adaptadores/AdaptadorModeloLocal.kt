package com.example.funny.com.proyecto.com.proyecto.adaptadores

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.funny.R
import com.example.funny.com.proyecto.beans.Club

import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.modelo_local.view.*

class AdaptadorModeloLocal(val locales: ArrayList<Club>, val context: Context?) :
    RecyclerView.Adapter<LocalViewHolder>(), View.OnClickListener
{
    private var listener : View.OnClickListener?=null


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): LocalViewHolder {

       val  view = LayoutInflater.from(context).inflate(R.layout.modelo_local,parent,false)

        //view.animation=AnimationUtils.loadAnimation(context,R.anim.mov_izq_der_animation)

        view.setOnClickListener(this)

        return LocalViewHolder(view)
    }

    override fun getItemCount(): Int {
        return locales.size
    }

    override fun onBindViewHolder(holder: LocalViewHolder, position: Int) {

        holder.binHolder(locales.get(position))
    }


    override fun onViewAttachedToWindow(holder: LocalViewHolder) {
        super.onViewAttachedToWindow(holder)
        //holder.itemView.animation=AnimationUtils.loadAnimation(context,R.anim.mov_izq_der_animation)
    }

    fun setOnClickListener(listener : View.OnClickListener)
    {
        this.listener=listener
    }

    override fun onClick(v: View?) {
        if(listener!=null)
        {
            listener?.onClick(v)
        }
    }

}

class LocalViewHolder(view: View) : RecyclerView.ViewHolder(view)
{
    private val fotoLocal = view.foto_local as ImageView
    private val nombreLocal = view.nombre_local as TextView
    private val descripcion = view.descrip_local as TextView
    private val distrito = view.distrito_local as TextView

    fun binHolder(local : Club)
    {
        //fotoLocal.setImageResource(local.fotoLocal)

        if(local.fotoClub.isEmpty())
        {

            local.fotoClub="https://pbs.twimg.com/profile_images/1062007558418907137/xZtVMnGz_400x400.jpg"
            Picasso.get().load(local.fotoClub).into(fotoLocal)
        }
        else
        {
            Picasso.get().load(local.fotoClub).into(fotoLocal)
        }

        nombreLocal.text=local.nombreClub
        descripcion.text=local.descripcion
        distrito.text=local.direccion
    }

}