package com.example.funny.com.proyecto.com.proyecto.adaptadores

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.TextView
import com.example.funny.R
import com.example.funny.com.proyecto.beans.Evento
import com.example.funny.com.proyecto.beans.com.proyecto.beans.listas.IntegranteLista
import java.util.zip.Inflater

class ModeloIntegranteLista (var lista : List<Evento>, var context: Context) : BaseAdapter()
{
    override fun getView(p0: Int, p1: View?, p2: ViewGroup?): View {

        var view = LayoutInflater.from(context).inflate(R.layout.modelo_item_integrante_lista, p2, false)
        var integranteViewHolder = IntegranteListaViewHolder(view)
        integranteViewHolder.nommbreEvento.text=lista.get(p0).nombreEvento



        return view
    }

    override fun getItem(p0: Int): Any {
        return lista.get(p0)
    }

    override fun getItemId(p0: Int): Long {
        return p0.toLong()
    }

    override fun getCount(): Int {
        return lista.size
    }


    private class IntegranteListaViewHolder(view : View)
    {
        lateinit var nommbreEvento  : TextView

        init {
            nommbreEvento = view.findViewById(R.id.txt_nombre_integrante_lista_item)
        }
    }

}