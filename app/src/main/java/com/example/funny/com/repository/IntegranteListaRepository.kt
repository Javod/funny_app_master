package com.example.funny.com.repository

import androidx.lifecycle.LiveData
import com.example.funny.com.daos.IntegranteListaDAO
import com.example.funny.com.proyecto.beans.com.proyecto.beans.listas.IntegranteLista

class IntegranteListaRepository(private val integranteListaDAO: IntegranteListaDAO) {


    val integranteListaLivedata : LiveData<List<IntegranteLista>> = integranteListaDAO.gatAllIntegrantes()

    suspend fun gatAllIntegrantes() : LiveData<List<IntegranteLista>>
    {
        return integranteListaLivedata
    }


    suspend fun save(integranteLista: IntegranteLista)
    {
       // InsertIntegranteListaAsyncTask(integranteListaDAO).execute(integranteLista)
        integranteListaDAO.save(integranteLista)

    }

    suspend fun update(integranteLista: IntegranteLista)
    {
        //UpdateIntegranteLista(integranteListaDAO).execute(integranteLista)
    }

    suspend fun delete(integranteLista: IntegranteLista)
    {
        //EliminarIntegranteLista(integranteListaDAO).execute(integranteLista)
    }

}