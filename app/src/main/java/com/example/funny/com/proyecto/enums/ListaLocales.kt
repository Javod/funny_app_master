package com.example.funny.com.proyecto.enums

import com.example.funny.com.proyecto.beans.Club


// Clase con los datos de locales
class ListaLocales()
{
    private var locales : ArrayList<Club>?= ArrayList()

    fun addLocales() : ArrayList<Club>?
    {
        locales?.add(Club(1,2,"Anticua","St. 2, Grp. 4", "https://i.pinimg.com/736x/92/89/fc/9289fc8660c97dedcd03879b3fd1cc8e.jpg","Discoteca Bar",true))
        locales?.add(Club(2,3,"Mamma","St. 4, Grp. 16", "https://mensandbeauty.com/wp-content/uploads/2016/01/mejores-discotecas-mundo-pacha-ibiza-espana.jpg","Discoteca Bar",true))
        locales?.add(Club(3,3,"Kajuna","Plaza Butters", "https://i.ytimg.com/vi/eL8X9MwbNoo/maxresdefault.jpg","Discoteca Bar",true))
        locales?.add(Club(4,3,"Mokka","St. 2, Grp. 4", "https://nochesenlima.com/wp-content/uploads/2012/12/amnesia_disco.jpg","Discoteca Bar",true))
        locales?.add(Club(5,3,"Kajuna","Plaza Butters", "https://pbs.twimg.com/profile_images/491991778115846144/XD8UCIEA_400x400.jpeg","Discoteca Bar",true))
        locales?.add(Club(6,4,"Mamma","St. 4, Grp. 16", "https://cdn5.f-cdn.com/contestentries/1314344/18104777/5ae4ab686b5cb_thumb900.jpg","Discoteca Bar",true))
        locales?.add(Club(7,3,"Kajuna","Plaza Butters", "https://centraldediseno.files.wordpress.com/2013/10/logotipo_home.jpg?w=788","Discoteca Bar",true))
        locales?.add(Club(8,3,"Mokka","St. 2, Grp. 4","https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQi5l8ancqb6rnVpuOTDnZwb8mz2mx2nRz6Dr5DkOLSNsFYHtKA","Discoteca Bar",true))
        return locales
    }
}