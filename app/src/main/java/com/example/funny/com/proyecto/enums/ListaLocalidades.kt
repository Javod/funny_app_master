package com.example.funny.com.proyecto.enums

class ListaLocalidades
{
    companion object
    {
        val VILLA_EL_SALVADOPR=1
        val VILLA_MARIA_DEL_TRIUNFO=2
        val BARRANCO=3
        val SAN_JUAN=4
        val CHORRILLOS=5
        val LURIN=6
        val MIRAFLORES=7
        val LA_MOLINA=8
        val SURCO=9
    }
}