package com.example.funny.com.proyecto.vistas.fragmentos

import android.content.Context
import android.net.Uri
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.ProgressBar
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.example.funny.R
import com.example.funny.com.proyecto.beans.Club
import com.example.funny.com.proyecto.com.proyecto.adaptadores.AdaptadorModeloLocal
import com.example.funny.com.proyecto.enums.ListaLocales
import com.example.funny.com.proyecto.enums.PuntosFinales
import com.example.funny.com.proyecto.retrofitconnetion.ServiceEndPoint
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import retrofit2.*
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"


class FragmentoListaLocales : Fragment() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null
    private var listener: OnFragmentInteractionListener? = null

    private var locales : ArrayList<Club> = ArrayList()
    private var adaptadorLocal : AdaptadorModeloLocal?=null
    private var reciclador : RecyclerView? =null
    private var manager : RecyclerView.LayoutManager? = null

    private var myCompositeDisposable: CompositeDisposable? = null
    private var refrescarClubes : SwipeRefreshLayout?=null

    lateinit var cargandoClub : LinearLayout
    lateinit var progress : ProgressBar


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val vista = inflater!!.inflate(R.layout.fragment_fragmento_lista_locales, container, false)
        initComponents(vista)
        getDataServiceClubs()

        refrescarClubes!!.setOnRefreshListener { getDataServiceClubs() }


        return vista
    }

    // TODO: Rename method, update argument and hook method into UI event
    fun onButtonPressed(uri: Uri) {
        listener?.onFragmentInteraction(uri)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnFragmentInteractionListener) {
            listener = context
        } else {
            throw RuntimeException(context.toString() + " must implement OnFragmentInteractionListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    override fun onPause() {
        super.onPause()
        //locales= ArrayList()
    }

    fun initComponents(view : View)
    {
        refrescarClubes = view.findViewById(R.id.refrescar_info_clubes)
        myCompositeDisposable = CompositeDisposable()
        reciclador = view.findViewById(R.id.reciclador)
        //adaptadorLocal = AdaptadorModeloLocal(locales,context)
        manager = LinearLayoutManager(context)
        reciclador?.layoutManager=manager
        //reciclador?.adapter=adaptadorLocal
        cargandoClub = view.findViewById(R.id.cargando_locales)
        progress = view.findViewById(R.id.cargando_l)

    }

    fun getDataServiceClubs()
    {
        val retrofit = Retrofit.Builder().baseUrl(PuntosFinales.urlRaiz).addConverterFactory(
            GsonConverterFactory.create()).addCallAdapterFactory(RxJava2CallAdapterFactory.create()).build();

        val restCliente = retrofit.create(ServiceEndPoint::class.java)

        myCompositeDisposable!!.add(restCliente.allClubs()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe(this::setDataRestClubes))

        refrescarClubes!!.isRefreshing=false

    }

    fun setDataRestClubes(clubes : List<Club>)
    {
        locales= ArrayList(clubes)
        if(locales.size!=0)
        {
            cargandoClub.visibility=View.GONE
            progress.isIndeterminate=false
            adaptadorLocal = AdaptadorModeloLocal(locales,context)
            reciclador?.adapter=adaptadorLocal

            adaptadorLocal?.setOnClickListener(View.OnClickListener {
                //mensaje("click")
                listener?.enviarLocal(locales.get(reciclador!!.getChildAdapterPosition(it)))
            })
        }

    }

    fun mensaje (msj : String)
    {
        Toast.makeText(context,msj, Toast.LENGTH_SHORT).show()
    }

    fun addLocales()
    {
        locales = ListaLocales().addLocales()!!
    }


    interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        fun onFragmentInteraction(uri: Uri)
        fun enviarLocal(local : Club)
    }

    companion object {

        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            FragmentoListaLocales().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }
}
