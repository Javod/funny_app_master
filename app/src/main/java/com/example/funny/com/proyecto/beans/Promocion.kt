package com.example.funny.com.proyecto.beans

import java.io.Serializable

data class Promocion(val idPromocion : Int,
                     val idLocal : Int,
                     val fotoPromocion : String,
                     val precio : Double,
                     val descrip : String,
                     val status : Boolean) : Serializable

