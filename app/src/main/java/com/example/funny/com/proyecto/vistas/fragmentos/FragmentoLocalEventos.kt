package com.example.funny.com.proyecto.vistas.fragmentos

import android.content.Context
import android.net.Uri
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.funny.R
import com.example.funny.com.proyecto.beans.Club
import com.example.funny.com.proyecto.beans.Evento
import com.example.funny.com.proyecto.com.proyecto.adaptadores.AdaptadorModeloEventoLocal
import com.example.funny.com.proyecto.enums.ListaLocales
import com.example.funny.com.proyecto.enums.PuntosFinales
import com.example.funny.com.proyecto.retrofitconnetion.ServiceEndPoint
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Activities that contain this fragment must implement the
 * [FragmentoLocalEventos.OnFragmentInteractionListener] interface
 * to handle interaction events.
 * Use the [FragmentoLocalEventos.newInstance] factory method to
 * create an instance of this fragment.
 *
 */
class FragmentoLocalEventos : Fragment(), AdaptadorModeloEventoLocal.SetInteractionRecyclerClick {



    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null
    private var listener: OnFragmentInteractionListener? = null

    private var eventos : ArrayList<Evento> = ArrayList()
    private var adaptadorEventos : AdaptadorModeloEventoLocal? = null
    private var reciclador : RecyclerView? =null
    private var manager : RecyclerView.LayoutManager? = null
    private var locales : ArrayList<Club>  = ArrayList()
    private lateinit var evento : Evento
    private var idClub =0;
    private var bandera = true

    private var myCompositeDisposable: CompositeDisposable? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }


    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        var view = inflater.inflate(R.layout.fragment_fragmento_local_eventos, container, false)

        initcomponents(view)

        if(arguments!=null)
        {
           if(arguments!!.getSerializable("evento_click")!=null)
           {
               evento = arguments!!.getSerializable("evento_click") as Evento
               getDataEventosRest(evento.idLocal)
               bandera=true
           }
            else
           {
               idClub = arguments!!.getInt("id_club")
               getDataEventosRest(idClub)
               bandera=false
           }
        }



        return view
    }

    // TODO: Rename method, update argument and hook method into UI event
    fun onButtonPressed(uri: Uri) {
        listener?.onFragmentInteraction(uri)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnFragmentInteractionListener) {
            listener = context
        } else {
            throw RuntimeException(context.toString() + " must implement OnFragmentInteractionListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    override fun onPause() {
        super.onPause()

        eventos = ArrayList()
    }

    fun initcomponents(view : View)
    {
        myCompositeDisposable = CompositeDisposable()
        locales = ListaLocales().addLocales()!!
        reciclador = view.findViewById(R.id.reciclador_eventos_local)
        //eventos = ListaEventos().addEventos()!!
        adaptadorEventos = AdaptadorModeloEventoLocal(this,eventos, context!!)
        manager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
        reciclador?.layoutManager=manager
        reciclador?.adapter=adaptadorEventos
    }

    fun getDataEventosRest(id : Int)
    {
        val retrofit = Retrofit.Builder().baseUrl(PuntosFinales.urlRaiz).addConverterFactory(
            GsonConverterFactory.create()).addCallAdapterFactory(RxJava2CallAdapterFactory.create()).build();

        val restCliente = retrofit.create(ServiceEndPoint::class.java)

        myCompositeDisposable!!.add(restCliente.eventosByILocal(id)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.newThread())
            .subscribe(this::dataEventosRest))

    }

    fun dataEventosRest(listaE : List<Evento>)
    {
        var listaE = ArrayList(listaE)
        if(bandera)
        {
            eventos.add(evento)
        }
        for(e in listaE!!)
        {
            if(e.fotoEvento.isEmpty())
            {
                e.fotoEvento="https://pbs.twimg.com/profile_images/1062007558418907137/xZtVMnGz_400x400.jpg"
                eventos!!.add(e)
            }
            else{
                eventos!!.add(e)
            }
        }

        adaptadorEventos = AdaptadorModeloEventoLocal(this,eventos, context!!)
        reciclador?.adapter=adaptadorEventos
    }

    override fun clickButtonCrearLista(nombreEvento: String, idEvento : Int) {

        listener!!.enviarNombrEvento(nombreEvento,idEvento )

    }




    interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        fun onFragmentInteraction(uri: Uri)
        fun enviarNombrEvento(nombre : String,idEvento : Int)


    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment FragmentoLocalEventos.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            FragmentoLocalEventos().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }
}
