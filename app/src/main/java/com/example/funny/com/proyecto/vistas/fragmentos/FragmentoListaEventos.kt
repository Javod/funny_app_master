package com.example.funny.com.proyecto.vistas.fragmentos

import android.content.Context
import android.net.Uri
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.ProgressBar
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.example.funny.R
import com.example.funny.com.proyecto.beans.Evento
import com.example.funny.com.proyecto.com.proyecto.adaptadores.AdaptadorModeloEvento
import com.example.funny.com.proyecto.enums.ListaEventos
import com.example.funny.com.proyecto.enums.PuntosFinales
import com.example.funny.com.proyecto.retrofitconnetion.ServiceEndPoint
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import io.reactivex.schedulers.Schedulers


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Activities that contain this fragment must implement the
 * [FragmentoListaEventos.OnFragmentInteractionListener] interface
 * to handle interaction events.
 * Use the [FragmentoListaEventos.newInstance] factory method to
 * create an instance of this fragment.
 *
 */
class FragmentoListaEventos : Fragment() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null
    private var listener: OnFragmentInteractionListener? = null

    private var eventos : ArrayList<Evento> = ArrayList()
    private var adaptadorEventos : AdaptadorModeloEvento? = null
    private var reciclador : RecyclerView? =null
    private var manager : RecyclerView.LayoutManager? = null
    private var myCompositeDisposable: CompositeDisposable? = null
    private var refrescarEventos : SwipeRefreshLayout?=null

    lateinit var cargandoEventos : LinearLayout
    lateinit var progress : ProgressBar

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val view = inflater.inflate(R.layout.fragment_fragmento_lista_eventos, container, false)
        initcomponents(view)

        getDataEventos()
        refrescarEventos!!.setOnRefreshListener {getDataEventos()}


        return view
    }

    // TODO: Rename method, update argument and hook method into UI event
    fun onButtonPressed(uri: Uri) {
        listener?.onFragmentInteraction(uri)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnFragmentInteractionListener) {
            listener = context
        } else {
            throw RuntimeException(context.toString() + " must implement OnFragmentInteractionListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    override fun onPause() {
        super.onPause()
        //eventos  = ArrayList()


    }

    fun getDataEventos()
    {

        val retrofit = Retrofit.Builder().baseUrl(PuntosFinales.urlRaiz).addConverterFactory(
            GsonConverterFactory.create()).addCallAdapterFactory(RxJava2CallAdapterFactory.create()).build();

        val restCliente = retrofit.create(ServiceEndPoint::class.java)

        myCompositeDisposable!!.add(restCliente.allEventos()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.newThread())
            .subscribe(this::dataEventosRest))

        refrescarEventos!!.isRefreshing=false

    }

    fun dataEventosRest(listaE : List<Evento>)
    {

        cargandoEventos.visibility=View.GONE
        reciclador!!.visibility=View.VISIBLE
        eventos= ArrayList()
        for(e in listaE!!)
        {
            if(e.fotoEvento.isEmpty())
            {
                e.fotoEvento="https://pbs.twimg.com/profile_images/1062007558418907137/xZtVMnGz_400x400.jpg"
                eventos!!.add(e)
            }
            else{
                eventos!!.add(e)
            }
        }

        adaptadorEventos = AdaptadorModeloEvento(eventos,context)
        reciclador?.adapter=adaptadorEventos

        adaptadorEventos?.setOnClickListener(View.OnClickListener {
            listener!!.mandarDatosEventos(eventos.get(reciclador!!.getChildAdapterPosition(it)))
        })
    }

    fun initcomponents(view : View)
    {
        myCompositeDisposable = CompositeDisposable()
        refrescarEventos = view.findViewById(R.id.refrescar_info_eventos)
        reciclador = view.findViewById(R.id.reciclador_eventos)
        //adaptadorEventos = AdaptadorModeloEvento(eventos,context)
        manager = LinearLayoutManager(context)
        reciclador?.layoutManager=manager

        cargandoEventos = view.findViewById(R.id.cargando_eventos)
        progress = view.findViewById(R.id.cargando)

    }

    fun addEventos()
    {
        eventos = ListaEventos().addEventos()!!
    }


    fun mensaje(msj : String)
    {
        Toast.makeText(context, msj, Toast.LENGTH_SHORT).show()
    }


    interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        fun onFragmentInteraction(uri: Uri)
        fun mandarDatosEventos(e : Evento)
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment FragmentoListaEventos.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            FragmentoListaEventos().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }


}
