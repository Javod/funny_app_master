package com.example.funny.com.proyecto.beans

import java.io.Serializable

data class Usuario (val idUsuario : Int,
                    val nombre : String,
                    val pass : String,
                    val correo :String,
                    val dni : String,
                    val fechaNac: String,
                    val sexo : String,
                    val status : Boolean) : Serializable