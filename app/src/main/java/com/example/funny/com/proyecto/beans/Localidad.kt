package com.example.funny.com.proyecto.beans

import java.io.Serializable

data class Localidad (
    val idLocalidad : Int,
    val idDepartamento :  Int,
    val localidadNombre : String,
    val status : Boolean) : Serializable{

}