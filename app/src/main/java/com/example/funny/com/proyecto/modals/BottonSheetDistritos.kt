package com.example.funny.com.proyecto.modals

import android.annotation.SuppressLint
import android.app.Dialog
import android.view.View
import android.widget.Button
import com.example.funny.R
import com.google.android.material.bottomsheet.BottomSheetDialogFragment


//Clase para crear  las vistas que surgen desde la
//parte inferior de la pantalla (BottonSheet)
class BottonSheetDistritos(val layoutStyle : Int) : BottomSheetDialogFragment() {


    lateinit var  btnElegirDistritos : Button

    @SuppressLint("RestrictedApi")
    override fun setupDialog(dialog: Dialog?, style: Int) {
        super.setupDialog(dialog, style)


        val view = View.inflate(context,this.layoutStyle,null)

        if(this.layoutStyle== R.layout.botton_sheet_promos)
        {
           /* btnElegirDistritos = view.findViewById(R.id.btn_elegir_distritos)

            btnElegirDistritos.setOnClickListener {
                dialog?.dismiss()
            }*/

        }

        dialog?.setContentView(view)
    }
}