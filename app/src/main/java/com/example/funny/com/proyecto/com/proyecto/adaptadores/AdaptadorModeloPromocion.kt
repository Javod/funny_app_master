package com.example.funny.com.proyecto.com.proyecto.adaptadores

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.funny.R
import com.example.funny.com.proyecto.beans.Promocion
import com.squareup.picasso.Picasso

class AdaptadorModeloPromocion(val promociones : ArrayList<Promocion>, val context : Context) :
    RecyclerView.Adapter<AdaptadorModeloPromocion.PromocionViewholder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PromocionViewholder {
        var view : View

        view = LayoutInflater.from(context).inflate(R.layout.modelo_promocion, parent, false)

        return PromocionViewholder(view)


    }

    override fun getItemCount(): Int {
        return promociones.size
    }

    override fun onBindViewHolder(holder: PromocionViewholder, position: Int) {
        holder.binHolder(promociones.get(position))
    }


    class PromocionViewholder(view : View) : RecyclerView.ViewHolder(view)
    {
        private var fotoPromocion = view.findViewById<ImageView>(R.id.foto_promocion)
        private var txtPrecioPromo = view.findViewById<TextView>(R.id.txt_precio_promocion)
        private var txtDescripPromo = view.findViewById<TextView>(R.id.txt_descrip_promocion)

        fun binHolder(promo : Promocion)
        {

            //fotoPromocion.setImageResource(promo.fotoPromocion)
            Picasso.get().load(promo.fotoPromocion).into(fotoPromocion)
            txtPrecioPromo.text="Precio: S/."+promo.precio
            txtDescripPromo.text=promo.descrip
        }

    }
}