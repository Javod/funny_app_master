package com.example.funny.com.daos

import androidx.lifecycle.LiveData
import androidx.room.*
import com.example.funny.com.proyecto.beans.com.proyecto.beans.listas.IntegranteLista

@Dao
interface IntegranteListaDAO {


    @Query("SELECT * FROM integrante_lista")
    fun gatAllIntegrantes() : LiveData<List<IntegranteLista>>

    @Insert
    suspend fun save(integrante : IntegranteLista)

    @Update
    suspend fun update(integrante : IntegranteLista)

    @Delete
    suspend fun delete(integrante: IntegranteLista)
}