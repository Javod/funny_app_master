package com.example.funny.com.repository

import android.os.AsyncTask
import androidx.lifecycle.LiveData
import com.example.funny.com.daos.ListaDAO
import com.example.funny.com.proyecto.beans.com.proyecto.beans.listas.Lista

class ListaRepository(val listaDAO: ListaDAO)
{

    var listaLiveData: LiveData<List<Lista>> = listaDAO.getAllListas()



    suspend fun gelAllListas() : LiveData<List<Lista>>
    {
        return listaLiveData
    }


    suspend fun save(lista: Lista)
    {
        //InsertListaAsyncTask(listaDAO).execute(lista)
        listaDAO.save(lista)
    }

    suspend fun update(lista: Lista)
    {
        //UpdateListaAsynTask(listaDAO).execute(lista)
        listaDAO.update(lista)
    }

    suspend fun eliminar(lista: Lista)
    {
        //EliminarListaAsynTask(listaDAO).execute(lista)
        listaDAO.delete(lista)
    }



}