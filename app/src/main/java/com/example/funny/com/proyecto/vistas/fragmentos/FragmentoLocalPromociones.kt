package com.example.funny.com.proyecto.vistas.fragmentos

import android.content.Context
import android.net.Uri
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.ProgressBar
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.funny.R
import com.example.funny.com.proyecto.beans.Promocion
import com.example.funny.com.proyecto.com.proyecto.adaptadores.AdaptadorModeloPromocion
import com.example.funny.com.proyecto.enums.ListaPromociones
import com.example.funny.com.proyecto.enums.PuntosFinales
import com.example.funny.com.proyecto.retrofitconnetion.ServiceEndPoint
import io.reactivex.Scheduler
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"


class FragmentoLocalPromociones : Fragment() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null
    private var listener: OnFragmentInteractionListener? = null

    private var adaptadorPromo : AdaptadorModeloPromocion?=null
    private var promociones : ArrayList<Promocion> =ArrayList()
    private var recicladorPromo : RecyclerView?=null
    private var managerPromos : RecyclerView.LayoutManager? = null
    private var sinPromos : TextView?= null

    private var myCompositeDisposable: CompositeDisposable? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        var view = inflater.inflate(R.layout.fragment_fragmento_local_promociones, container, false)

        initComponentsPromos(view)

        if(arguments!=null)
        {
            if(arguments!!.getInt("idClub")!=null)
            {
                getDataPromocionesrest(arguments!!.getInt("idClub"))
            }
        }


        return view
    }

    // TODO: Rename method, update argument and hook method into UI event
    fun onButtonPressed(uri: Uri) {
        listener?.onFragmentInteraction(uri)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnFragmentInteractionListener) {
            listener = context
        } else {
            throw RuntimeException(context.toString() + " must implement OnFragmentInteractionListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }



    fun initComponentsPromos(view : View)
    {
        promociones = ListaPromociones().addPromociones()!!
        recicladorPromo= view.findViewById(R.id.reciclador_promociones)
        managerPromos = LinearLayoutManager(context)
        recicladorPromo?.layoutManager=managerPromos
        sinPromos = view.findViewById(R.id.mensaje_sin_promos)
        myCompositeDisposable = CompositeDisposable()


    }

    fun getDataPromocionesrest(idClub : Int)
    {
        val retrofit = Retrofit.Builder().baseUrl(PuntosFinales.urlRaiz).addConverterFactory(
            GsonConverterFactory.create()).addCallAdapterFactory(RxJava2CallAdapterFactory.create()).build();

        val restCliente = retrofit.create(ServiceEndPoint::class.java)

        myCompositeDisposable!!.add(restCliente.getPromocionesbyIdClub(idClub)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.newThread())
            .subscribe(this::setDataPromociones))
    }

    fun setDataPromociones(promociones : List<Promocion>)
    {
        this.promociones= ArrayList(promociones)

        if(this.promociones!=null)
        {
            sinPromos!!.visibility=View.GONE
            adaptadorPromo= AdaptadorModeloPromocion(this.promociones,context!!)
            recicladorPromo?.adapter=adaptadorPromo
        }
        else
        {
            recicladorPromo!!.visibility=View.GONE
        }

    }


    interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        fun onFragmentInteraction(uri: Uri)
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment FragmentoLocalPromociones.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            FragmentoLocalPromociones().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }
}
