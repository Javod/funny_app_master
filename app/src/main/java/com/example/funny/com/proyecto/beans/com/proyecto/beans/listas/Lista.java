package com.example.funny.com.proyecto.beans.com.proyecto.beans.listas;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.io.Serializable;

@Entity(tableName = "lista")
public class Lista implements Serializable {


    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id_lista")
    private int idLista;

    @ColumnInfo(name = "id_evento")
    private int idEvento;

    @ColumnInfo(name = "id_usuario")
    private int idUsuario;


    @ColumnInfo(name = "nombre_lista")
    private String nombreLista;


    @ColumnInfo(name="cod_lista")
    private String codLista;


    @ColumnInfo(name="fecha")
    private String fecha;

    @ColumnInfo(name="status")
    private boolean status;

    public Lista()
    {

    }

    public Lista(int id, int idEvento, int idUsuario, String nombreLista, String codLista, String fecha, boolean status) {
        this.idLista = id;
        this.idEvento = idEvento;
        this.idUsuario = idUsuario;
        this.nombreLista = nombreLista;
        this.codLista = codLista;
        this.fecha = fecha;
        this.status=status;
    }




    public int getIdLista() {
        return idLista;
    }

    public void setIdLista(int id) {
        this.idLista = id;
    }

    public int getIdEvento() {
        return idEvento;
    }

    public void setIdEvento(int idEvento) {
        this.idEvento = idEvento;
    }

    public int getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(int idUsuario) {
        this.idUsuario = idUsuario;
    }

    public String getNombreLista() {
        return nombreLista;
    }

    public void setNombreLista(String nombreLista) {
        this.nombreLista = nombreLista;
    }

    public String getCodLista() {
        return codLista;
    }

    public void setCodLista(String codLista) {
        this.codLista = codLista;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }
}
