package com.example.funny.com.proyecto.com.proyecto.adaptadores

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.TextView
import com.example.funny.R
import com.example.funny.com.proyecto.beans.com.proyecto.beans.listas.Lista
import kotlinx.android.synthetic.main.modelo_item_lista_user.view.*

class ModeloListaUser(var lista : List<Lista>, var context : Context) :BaseAdapter()
{
    override fun getView(p0: Int, p1: View?, p2: ViewGroup?): View {
        var view : View?

        view = LayoutInflater.from(context).inflate(R.layout.modelo_item_lista_user, p2, false)
        var listaViewHolder = ListaViewHolder(view)

        listaViewHolder.txtNombreLista.text=lista.get(p0).nombreLista
        listaViewHolder.txtCodLista.text = lista.get(p0).codLista

        return  view

    }

    override fun getItem(p0: Int): Any {
        return lista.get(p0)
    }

    override fun getItemId(p0: Int): Long {

        return p0.toLong()
    }

    override fun getCount(): Int {
        return lista.size
    }

    private class ListaViewHolder(view : View)
    {
        lateinit var txtNombreLista : TextView
        lateinit var txtCodLista : TextView

        init {
            txtNombreLista = view.findViewById(R.id.txt_lista_evento_user_nombre)
            txtCodLista = view.findViewById(R.id.txt_lista_evento_user_codigo)
        }
    }

}