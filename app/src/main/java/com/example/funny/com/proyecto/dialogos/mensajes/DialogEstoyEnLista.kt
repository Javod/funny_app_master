package com.example.funny.com.proyecto.dialogos.mensajes

import android.app.Activity
import android.app.AlertDialog
import android.app.Dialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.widget.ImageButton
import android.widget.ImageView
import android.widget.TextView
import androidx.fragment.app.DialogFragment
import com.example.funny.R
import com.example.funny.com.proyecto.beans.Evento
import com.example.funny.com.proyecto.utils.ConversorFormatoFecha
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_user.view.*

class DialogEstoyEnLista(var evento : Evento, var listener : CerrarDialogEstoyEnLista) : DialogFragment()
{

    lateinit var btnCerrarDialog : ImageButton

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        var view = activity!!.layoutInflater.inflate(R.layout.dialog_modelo_estoy_en_lista, null)

        btnCerrarDialog = view.findViewById(R.id.btn_cerrar_estoy_en_lista)
        var bindViewholder = DialogEstoyEnListaViewholder(view)
        bindViewholder.viewDatosEventos(evento)


        btnCerrarDialog.setOnClickListener({listener.cerrarDialogEstoyEnLista()})

        var dialog = AlertDialog.Builder(activity)
        dialog.setView(view)


        return dialog.create()
    }

    private class DialogEstoyEnListaViewholder(view : View)
    {
        private var imagen : ImageView
        private var nombre : TextView
        private var descrip : TextView
        private var mes : TextView
        private var dia : TextView
        private var precio : TextView
        private var aforo : TextView

        init {
            imagen = view.findViewById(R.id.foto_evento_local_s)
            nombre = view.findViewById(R.id.nombre_evento_vista_s)
            descrip = view.findViewById(R.id.txt_descrip_evento_s)
            mes = view.findViewById(R.id.txt_fecha_evento_mes_s)
            dia = view.findViewById(R.id.txt_fecha_evento_dia_s)
            precio = view.findViewById(R.id.txt_precio_entrada_s)
            aforo  =view.findViewById(R.id.txt_aforo_s)
        }

        fun viewDatosEventos(evento : Evento)
        {
            var conversor = ConversorFormatoFecha(evento.fecha)

            nombre.text=evento.nombreEvento
            descrip.text=evento.descripcion
            mes.text= conversor.nombreMes()
            dia.text=""+conversor.diaEvento()
            precio.text = "Precio: S/. ${evento.precioEntrada}"
            if(evento.fotoEvento.isEmpty())
            {
                Picasso.get().load("https://pbs.twimg.com/profile_images/1062007558418907137/xZtVMnGz_400x400.jpg").into(imagen)
            }
            else{
                Picasso.get().load(evento.fotoEvento).into(imagen)
            }

            aforo.text="Lugar : ${evento.nombreClub}"
        }
    }

    interface CerrarDialogEstoyEnLista
    {
       fun cerrarDialogEstoyEnLista()
    }
}