package com.example.funny.com.proyecto.com.proyecto.adaptadores

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.funny.R
import com.example.funny.com.proyecto.beans.Evento
import com.example.funny.com.proyecto.utils.ConversorFormatoFecha
import com.squareup.picasso.Picasso

class AdaptadorModeloEvento(val eventos : ArrayList<Evento>, val context : Context?) :
    RecyclerView.Adapter<AdaptadorModeloEvento.EventoViewHolder>(), View.OnClickListener{

    private var listener : View.OnClickListener?=null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): EventoViewHolder {
        val view : View
        view = LayoutInflater.from(context).inflate(R.layout.modelo_evento,parent,false)
        view.setOnClickListener(this)

        return EventoViewHolder(view)
    }

    override fun getItemCount(): Int {
        return eventos.size
    }

    override fun onBindViewHolder(holder: EventoViewHolder, position: Int) {

        holder.binHolder(eventos.get(position))

    }

    override fun onViewAttachedToWindow(holder: EventoViewHolder) {
        super.onViewAttachedToWindow(holder)
        //holder.itemView.animation= AnimationUtils.loadAnimation(context,R.anim.mov_izq_der_animation)
    }

    fun setOnClickListener(listener : View.OnClickListener)
    {
        this.listener=listener
    }

    override fun onClick(v: View?) {
        if(listener!=null)
        {
            listener?.onClick(v)
        }
    }

    class EventoViewHolder(view : View) : RecyclerView.ViewHolder(view)
    {
        private val fotoEvento = view.findViewById<ImageView>(R.id.foto_evento)
        private val nombreEvento = view.findViewById<TextView>(R.id.nombre_evento)
        private val localEvento = view.findViewById<TextView>(R.id.local_evento)
        private val descrip = view.findViewById<TextView>(R.id.descrip_evento)
        private val diaEvento =view.findViewById<TextView>(R.id.dia_evento)
        private val mesEvento = view.findViewById<TextView>(R.id.mes_evento)

        fun binHolder(evento : Evento)
        {
            var conversorFormatoFecha = ConversorFormatoFecha(evento.fecha)
            //fotoEvento.setImageResource(evento.fotoEvento)
            Picasso.get().load(evento.fotoEvento).into(fotoEvento)
            nombreEvento.text=evento.nombreEvento
            localEvento.text=evento.nombreClub
            descrip.text = evento.descripcion
            diaEvento.text = "${conversorFormatoFecha.diaEvento()}"
            mesEvento.text=conversorFormatoFecha.nombreMes()
        }
    }

}