package com.example.funny.com.proyecto.com.proyecto.adaptadores

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import android.widget.*
import androidx.recyclerview.widget.RecyclerView
import com.example.funny.R
import com.example.funny.com.proyecto.beans.Evento
import com.example.funny.com.proyecto.utils.ConversorFormatoFecha
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.modelo_evento_local_vista.view.*

class AdaptadorModeloEventoLocal(val listenerButton : SetInteractionRecyclerClick, val eventos : ArrayList<Evento>, val context :Context) :
    RecyclerView.Adapter<AdaptadorModeloEventoLocal.ModeloEventoLocalViewHolder>(), View.OnClickListener
{
    private var listener : View.OnClickListener?=null



    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ModeloEventoLocalViewHolder {
        var view = LayoutInflater.from(context).inflate(R.layout.modelo_evento_local_vista, parent,false)


        view.setOnClickListener(this)
        return ModeloEventoLocalViewHolder(view)
    }

    override fun getItemCount(): Int {
        return eventos.size
    }


    override fun onBindViewHolder(holder: ModeloEventoLocalViewHolder, position: Int) {
        holder.binHolder(eventos.get(position))
        holder.btnCrearLista.setOnClickListener(View.OnClickListener {
            listenerButton!!.clickButtonCrearLista(eventos.get(position).nombreEvento,eventos.get(position).idEvento)
        })

        holder.btnConseguirEntrada.setOnClickListener(View.OnClickListener { mensaje("Esta función está en desarrollo") })


    }

    override fun onViewAttachedToWindow(holder: ModeloEventoLocalViewHolder) {
        super.onViewAttachedToWindow(holder)
        holder.itemView.animation= AnimationUtils.loadAnimation(context,R.anim.abc_slide_in_bottom)

    }

    fun setOnClickListener(listener : View.OnClickListener)
    {
        this.listener=listener
    }


    override fun onClick(v: View?) {
        if(listener!=null)
        {
            listener?.onClick(v)
        }
    }

    fun mensaje(msj : String )
    {
        Toast.makeText(context, msj,Toast.LENGTH_SHORT).show()
    }


    interface SetInteractionRecyclerClick
    {
        fun clickButtonCrearLista(nombreEvento : String, idEvento : Int)
    }


    class ModeloEventoLocalViewHolder(view : View) : RecyclerView.ViewHolder(view)
    {
        private val fotoEventoLocal  = view.foto_evento_local as ImageView
        private val txtNombreEvento  = view.nombre_evento_vista as TextView
        private val txtFechaEventoMes   = view.txt_fecha_evento_mes as TextView
        private val txtFechaEventoDia   = view.txt_fecha_evento_dia as TextView
        private val txtPrecioEntrada = view.txt_precio_entrada as TextView
        private val txtDescripcion   = view.txt_descrip_evento as TextView
        val btnCrearLista = view.btn_crear_lista as Button
        val btnConseguirEntrada = view.btn_conseguir_entradas as Button


        fun binHolder(evento : Evento)
        {
            //fotoEventoLocal.setImageResource(evento.fotoEvento)
            var conversor = ConversorFormatoFecha(evento.fecha)
            Picasso.get().load(evento.fotoEvento).into(fotoEventoLocal)
            txtNombreEvento.text=evento.nombreEvento
            txtFechaEventoMes.text = conversor.nombreMes()
            txtFechaEventoDia.text = "${conversor.diaEvento()}"
            txtPrecioEntrada.text = "Precio: S/." + evento.precioEntrada
            txtDescripcion.text=evento.descripcion
        }

        fun enviarNombre() : String
        {
            return  txtNombreEvento.text.toString()
        }


    }

}

