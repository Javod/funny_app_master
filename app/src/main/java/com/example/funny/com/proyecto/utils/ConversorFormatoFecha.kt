package com.example.funny.com.proyecto.utils

class ConversorFormatoFecha(var fecha: String) {

    private var mes =0;
    private  var dia = 0;
    private var anio = 0

    init {
        separarFecha()
    }
    private fun separarFecha()
    {
        var fechaArray = fecha.split("-")

        anio = Integer.valueOf(fechaArray[0])
        mes =  Integer.valueOf(fechaArray[1])
        dia = Integer.valueOf(fechaArray[2])
    }

    public fun nombreMes() : String{
        var  nMes = "nada"
        when(mes)
        {
            1->nMes="Ene."
            2->nMes="Feb."
            3->nMes="May."
            4->nMes="Abr."
            5->nMes="May."
            6->nMes="Jun."
            7->nMes="Jul."
            8->nMes="Ago."
            9->nMes="Sep."
            10->nMes="Oct."
            11->nMes="Nov."
            12->nMes="Dic."
        }

        return nMes
    }

    public fun diaEvento() : Int = dia

    public fun anioEvento() : Int = anio
}