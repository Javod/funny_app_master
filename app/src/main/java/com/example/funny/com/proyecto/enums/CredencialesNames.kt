package com.example.funny.com.proyecto.enums

// Clase con constantes para las credenciales del usuario
class CredencialesNames()
{
    val NOMBRE_DATA="CREDENCIALES"
    val NOMBRE="NOMBRE"
    val CORREO = "CORREO"
    val PASS = "PASS"
    val SEXO  ="S"
    val DNI = "DNI"
    val FECHA_NAC="FECHA_NAC"
    val CAMPO_VACIO="vacio"
    val STATUS = "STATUS";
    val ID_USUARIO="ID_USUARIO"
}
