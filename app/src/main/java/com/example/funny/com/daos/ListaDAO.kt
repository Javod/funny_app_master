package com.example.funny.com.daos

import androidx.lifecycle.LiveData
import androidx.room.*
import com.example.funny.com.proyecto.beans.com.proyecto.beans.listas.Lista

@Dao
interface ListaDAO {

    @Query("SELECT * FROM lista")
    fun getAllListas(): LiveData<List<Lista>>

    @Insert
    suspend fun save(lista : Lista)

    @Update
    suspend fun update(lista : Lista)

    @Delete
    suspend fun delete(lista : Lista)

}