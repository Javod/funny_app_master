package com.example.funny.com.proyecto.dialogos.mensajes

import android.app.AlertDialog
import android.app.Dialog
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.TextView
import androidx.fragment.app.DialogFragment
import com.example.funny.R

class DialogComfirmation(val txtcodigo : String, val listaner : InteractionCerrarDialog) : DialogFragment()
{

    var txtCodigoCrearLista : TextView?=null

    var btnAceptaCrearLista : Button? = null



    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        var view = activity!!.layoutInflater.inflate(R.layout.dialog_comfirmation_crear_lista, null)

        txtCodigoCrearLista = view.findViewById(R.id.txt_codigo_lista_dialog)
        btnAceptaCrearLista =view.findViewById(R.id.btn_aceptar_crear_lista)

        txtCodigoCrearLista!!.text=txtcodigo

        btnAceptaCrearLista!!.setOnClickListener(View.OnClickListener { listaner.cerrarDialog("dato") })

        val alertDialog = AlertDialog.Builder(activity)

        alertDialog.setView(view)




        return alertDialog.create()
    }

    interface InteractionCerrarDialog
    {
        fun cerrarDialog(dato : String)
    }
}