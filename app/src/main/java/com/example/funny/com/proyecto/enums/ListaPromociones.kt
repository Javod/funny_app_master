package com.example.funny.com.proyecto.enums

import com.example.funny.com.proyecto.beans.Promocion


//Clase con los datos de promociones
class ListaPromociones()
{
    private var promociones : ArrayList<Promocion> = ArrayList()

    fun addPromociones() :ArrayList<Promocion>?
    {
        promociones?.add(Promocion(1,2, "https://i.pinimg.com/originals/39/d8/05/39d805c0d188d1bc9b783ed2478620b2.jpg\n",50.00,"Un valde  con 6 Coronas heladas",true))
        promociones?.add(Promocion(2,1, "https://pbs.twimg.com/media/DL8md6_X4AANXs-.jpg\n",99.00,"Un valde  con 6 Cusqueñas heladas",true))
        promociones?.add(Promocion(3,2, "https://riviera.com.pe/wp-content/uploads/2019/06/post-27-03.png",50.00,"Un valde  con 6 Pilsen heladas",true))
        promociones?.add(Promocion(4,2, "http://www.lacasadelasalsa.com.pe/eventos/22agosto.jpg",380.00,"Box para 4 personas mas Jhonie Walker black",true))
        promociones?.add(Promocion(5,3, "https://www.discotecanick.com/imagenes/portada/portada1.jpg",100.00,"Valde con 10 Pilsen heladas",true))
        promociones?.add(Promocion(6,4, "http://www.soycordoba.es/wp-content/uploads/2015/01/10887478_839131022791798_129469640427506199_o.jpg",150.00,"Jhonnei Walker black label + guarana  y hielo",true))
        return promociones!!
    }

}