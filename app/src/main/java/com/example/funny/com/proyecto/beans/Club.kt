package com.example.funny.com.proyecto.beans

import java.io.Serializable

data class Club(val idAdministrador :Int,
                val idLocalidad: Int,
                val nombreClub : String,
                val direccion :String,
                var fotoClub :String,
                val descripcion :String,
                val status : Boolean) : Serializable{
}

