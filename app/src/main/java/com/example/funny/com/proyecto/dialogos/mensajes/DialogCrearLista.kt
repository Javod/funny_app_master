package com.example.funny.com.proyecto.dialogos.mensajes

import android.app.AlertDialog
import android.app.Dialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.TextureView
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ProgressBar
import android.widget.TextView
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.FragmentManager
import com.example.funny.R

class DialogCrearLista(val listener : InteractionDialog) : DialogFragment()
{
    var nombreEvento : String?="Evento"




    private var txtNombreEvento : TextView?=null
    private var progressBar : ProgressBar?=null
    var btnCrearListaDialog : Button? =  null
    private var txtCreandoLista  : TextView? = null




    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        var view = activity!!.layoutInflater.inflate(R.layout.dialog_crear_lista, null)

        txtNombreEvento = view.findViewById(R.id.txt_nombre_evento)
        progressBar = view.findViewById(R.id.progressBar)
        btnCrearListaDialog = view.findViewById(R.id.btn_crear_lista_dialog)
        txtCreandoLista = view.findViewById(R.id.txt_creando_lista)

        txtNombreEvento!!.text="¿Deseas crear una lista para el evento ${nombreEvento}?"



        btnCrearListaDialog!!.setOnClickListener(View.OnClickListener {

            cargarDatos()

            it.isEnabled=false
            listener!!.enviarAccion(true)

        })

        val alertDialog = AlertDialog.Builder(activity)

        alertDialog.setView(view)


        return alertDialog.create()
    }



    fun cargarDatos()
    {
        progressBar!!.visibility=View.VISIBLE
        txtCreandoLista!!.visibility=View.VISIBLE
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

    }


    interface InteractionDialog
    {
        fun enviarAccion(action:Boolean)
    }



}