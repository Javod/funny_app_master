package com.example.funny.com.proyecto.beans

import java.io.Serializable

data class Evento(
    val idEvento: Int,
    val idLocal: Int,
    val nombreEvento: String,
    val nombreClub: String,
    val fecha:String,
    val descripcion: String,
    val precioEntrada: Double,
    var fotoEvento: String,
    val status : Boolean) : Serializable
{

}