package com.example.funny.com.proyecto.com.proyecto.local.vista.tab

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter

class ViewPagerAdapterLocal(manager : FragmentManager) : FragmentPagerAdapter(manager) {

    val listaFramgentos : ArrayList<Fragment> = ArrayList()
    val listaNombres : ArrayList<String> = ArrayList()


    override fun getItem(position: Int): Fragment {

        return listaFramgentos.get(position)
    }

    override fun getCount(): Int {
        return listaFramgentos.size
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return listaNombres.get(position)

    }

    fun addFragment(fragmento : Fragment, titulo : String)
    {
        listaFramgentos.add(fragmento)
        listaNombres.add(titulo)
    }
}