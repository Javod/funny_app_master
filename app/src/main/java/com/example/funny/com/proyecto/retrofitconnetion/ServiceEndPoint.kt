package com.example.funny.com.proyecto.retrofitconnetion

import com.example.funny.com.proyecto.beans.*
import com.example.funny.com.proyecto.beans.com.proyecto.beans.listas.IntegranteLista
import com.example.funny.com.proyecto.beans.com.proyecto.beans.listas.Lista
import com.example.funny.com.proyecto.enums.ListaEventos
import com.example.funny.com.proyecto.enums.PuntosFinales
import io.reactivex.Observable
import retrofit2.Call
import com.example.funny.com.proyecto.utils.Response;
import retrofit2.http.*

interface ServiceEndPoint {

    @GET(PuntosFinales.allClubes)
    fun allClubs(): Observable<List<Club>>

    @GET(PuntosFinales.clubesByIdLocalidad + "{idLocalidad}")
    fun clubesByIdLocalidad(@Path("idLocalidad") idLocalidad: Int): Call<List<Club>>

    @GET(PuntosFinales.clubByIdClub + "{idClub}")
    fun clubByIdClub(@Path("idClub") idClub: Int): Observable<Club>

    @GET(PuntosFinales.allEventos)
    fun allEventos(): Observable<List<Evento>>

    @GET(PuntosFinales.eventosByIntegrante+"{idUsuario}")
    fun eventosByIntegrante(@Path("idUsuario") idUsuario : Int) : Observable<List<Evento>>


    @GET(PuntosFinales.promocionById + "{idPromocion}")
    fun promocionById(@Path("idPromocion") idPromocion: Int): Call<Promocion>

    @GET(PuntosFinales.allLocalidades)
    fun allLocalidades(): Call<List<Localidad>>

    @GET(PuntosFinales.localidadById + "{idLocalidad}")
    fun localidadById(@Path("idLocalidad") idLocalidad: Int): Call<Localidad>


    @GET(PuntosFinales.eventosByIdLocal + "{idLocal}")
    fun eventosByILocal(@Path("idLocal") idLocal: Int): Observable<List<Evento>>

    @GET(PuntosFinales.eventoByIdEvento + "{idEvento}")
    fun eventoByIdEvento(@Path("idEvento") idEvento: Int): Observable<Evento>


    @GET(PuntosFinales.listaByCodLista+"{codLista}")
    fun listaByCodLista(@Path("codLista") codLista : String) : Observable<Lista>


    @POST(PuntosFinales.listaPost)
    fun postLista(@Body lista: Lista): Observable<Lista>

    @POST(PuntosFinales.postIntegrante)
    fun postIntegrante(@Body integrante : IntegranteLista) : Observable<IntegranteLista>

    @GET(PuntosFinales.usuaiosEnLista+"{idLista}")
    fun getUsuariosEnLista(@Path("idLista") idLista : Int) : Observable<List<Usuario>>

    @POST(PuntosFinales.validarLoginUser)
    fun postRegistrarUsuario(@Body user : Usuario) : Observable<Response>

    @GET(PuntosFinales.promocionesByIdClub+"{idClub}")
    fun getPromocionesbyIdClub(@Path("idClub") idClub : Int) : Observable<List<Promocion>>

    @PUT(PuntosFinales.usuarioPut)
    fun putDatosUsuario(@Body user: Usuario) : Observable<Response>
}