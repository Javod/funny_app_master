package com.example.funny.com.viewmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.viewModelScope
import com.example.funny.com.databases.EventosDatabase
import com.example.funny.com.proyecto.beans.com.proyecto.beans.listas.Lista
import com.example.funny.com.repository.ListaRepository
import kotlinx.coroutines.launch

class ListaViewModel(application: Application) : AndroidViewModel(application)
{
    private val repository  : ListaRepository

    val getAllListas : LiveData<List<Lista>>

    init {
        val listaDAO = EventosDatabase.getDataBase(application).listaDAO()
        repository = ListaRepository(listaDAO)

        getAllListas = repository.listaLiveData
    }


    fun save(lista: Lista) = viewModelScope.launch {
        repository.save(lista)
    }

    fun update(lista: Lista) = viewModelScope.launch {
        repository.update(lista)
    }

    fun eliminar(lista: Lista) = viewModelScope.launch {
        repository.eliminar(lista)
    }
}