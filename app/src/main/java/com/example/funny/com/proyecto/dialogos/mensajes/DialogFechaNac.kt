package com.example.funny.com.proyecto.dialogos.mensajes

import android.app.DatePickerDialog
import android.app.Dialog
import android.os.Bundle
import android.widget.DatePicker
import androidx.fragment.app.DialogFragment
import java.util.*

class DialogFechaNac : DialogFragment(),DatePickerDialog.OnDateSetListener
{


    private var listener : DatePickerDialog.OnDateSetListener?=null


    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {

        val c= Calendar.getInstance()
        val y = c.get(Calendar.YEAR)
        val m = c.get(Calendar.MONTH)+1
        val d = c.get(Calendar.DAY_OF_MONTH)

        return DatePickerDialog(activity,this,y,m,d)
    }

    companion object{
        fun newInstance(listener : DatePickerDialog.OnDateSetListener) : DialogFechaNac
        {
            val fragment = DialogFechaNac()
            fragment.listener = listener

            return fragment
        }
    }

    override fun onDateSet(p0: DatePicker?, p1: Int, p2: Int, p3: Int) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }



}