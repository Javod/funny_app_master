package com.example.funny.com.proyecto.beans.com.proyecto.beans.listas

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import java.io.Serializable

@Entity(tableName = "integrante_lista")
data class IntegranteLista(
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id") var id : Int,
    @ColumnInfo(name = "id_lista") var idLista : Int,
    @ColumnInfo(name = "id_usuario") var idUsuario : Int,
    @ColumnInfo(name = "status") var status : Boolean
    ) : Serializable
{

}