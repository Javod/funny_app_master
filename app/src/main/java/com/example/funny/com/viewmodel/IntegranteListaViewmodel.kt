package com.example.funny.com.viewmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.viewModelScope
import com.example.funny.com.databases.EventosDatabase
import com.example.funny.com.proyecto.beans.com.proyecto.beans.listas.IntegranteLista
import com.example.funny.com.repository.IntegranteListaRepository
import kotlinx.coroutines.launch

class IntegranteListaViewmodel(application: Application): AndroidViewModel(application)
{


    private val repository : IntegranteListaRepository



    val getAllIntegrantesLista : LiveData<List<IntegranteLista>>

    init {
        val integranteListaDAO = EventosDatabase.getDataBase(application).integranteListaDAO()
        repository = IntegranteListaRepository(integranteListaDAO)
        getAllIntegrantesLista = repository.integranteListaLivedata
    }


    fun save(integranteLista: IntegranteLista) = viewModelScope.launch{

        repository.save(integranteLista)
    }

    fun update(integranteLista: IntegranteLista)= viewModelScope.launch{

        repository.update(integranteLista)
    }

    fun eliminar(integranteLista: IntegranteLista) =viewModelScope.launch {

        repository.delete(integranteLista)
    }



}