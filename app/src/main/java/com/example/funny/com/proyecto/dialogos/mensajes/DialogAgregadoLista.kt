package com.example.funny.com.proyecto.dialogos.mensajes

import android.app.AlertDialog
import android.app.Dialog
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.TextView
import androidx.fragment.app.DialogFragment
import com.example.funny.R

class DialogAgregadoLista(val nombreUsuario : String, val listener : InteractionDialogAgregadoLista) : DialogFragment()
{

    lateinit var txtNombreCreadorLista : TextView
    lateinit var btnAceptar : Button




    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        var view = activity!!.layoutInflater.inflate(R.layout.dialog_agregado_lista, null)

        txtNombreCreadorLista = view.findViewById(R.id.txt_nombre_creador_lista)
        btnAceptar = view.findViewById(R.id.btn_aceptar_agregado_lista)

        txtNombreCreadorLista.text = nombreUsuario

        btnAceptar.setOnClickListener(View.OnClickListener {
            listener.cerrarDialogAgregadoLista("mensaje cualquiera")
        })

        var dialog = AlertDialog.Builder(activity)

        dialog.setView(view)

        return dialog.create()
    }


    interface InteractionDialogAgregadoLista
    {
        fun cerrarDialogAgregadoLista(dato : String)
    }
}