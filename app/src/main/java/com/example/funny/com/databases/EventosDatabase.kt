package com.example.funny.com.databases

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.example.funny.com.daos.IntegranteListaDAO
import com.example.funny.com.daos.ListaDAO
import com.example.funny.com.proyecto.beans.com.proyecto.beans.listas.IntegranteLista
import com.example.funny.com.proyecto.beans.com.proyecto.beans.listas.Lista

@Database(entities = arrayOf(IntegranteLista::class,Lista::class), version = 9)
public abstract class EventosDatabase : RoomDatabase()
{

    abstract fun integranteListaDAO() : IntegranteListaDAO
    abstract fun listaDAO() : ListaDAO

    companion object{

        @Volatile
        private var INSTANCE : EventosDatabase? = null

        fun getDataBase(context: Context) : EventosDatabase
        {
            val tempInstance = INSTANCE

            if (tempInstance!=null)
            {
                return tempInstance
            }
            synchronized(this){
                val instance = Room.databaseBuilder(context.applicationContext,EventosDatabase::class.java,"eventos_database").fallbackToDestructiveMigration().build()
                INSTANCE = instance
                return  instance
            }
        }
    }

}