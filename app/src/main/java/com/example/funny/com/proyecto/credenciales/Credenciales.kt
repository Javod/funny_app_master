package com.example.funny.com.proyecto.credenciales

import android.content.Context
import com.example.funny.com.proyecto.beans.Usuario
import com.example.funny.com.proyecto.enums.CredencialesNames

//Clase para guardar las credenciales del usuario
class Credenciales(context : Context)
{

    val credencialesNames =CredencialesNames()

    val preferences = context.getSharedPreferences(credencialesNames.NOMBRE_DATA, Context.MODE_PRIVATE)


    fun existUsuario():Boolean
    {
        var existe = true
        if(preferences.getString(credencialesNames.NOMBRE,credencialesNames.CAMPO_VACIO)==credencialesNames.CAMPO_VACIO)
        {
            existe=false
        }
        return existe
    }

    fun getDataUser() : Usuario?
    {
        val user = Usuario(preferences.getInt(credencialesNames.ID_USUARIO,-1),
            preferences.getString(credencialesNames.NOMBRE, credencialesNames.CAMPO_VACIO),
            preferences.getString(credencialesNames.PASS,credencialesNames.CAMPO_VACIO),
            preferences.getString(credencialesNames.CORREO,credencialesNames.CAMPO_VACIO),
            preferences.getString(credencialesNames.DNI,credencialesNames.CAMPO_VACIO),
            preferences.getString(credencialesNames.FECHA_NAC,credencialesNames.CAMPO_VACIO),
            preferences.getString(credencialesNames.SEXO.toString(), credencialesNames.CAMPO_VACIO),
            true)

        return user
    }

    fun getDataUserId(): Int{
        return preferences.getInt(credencialesNames.ID_USUARIO,-1)
    }

    fun getDataUserName() : String{
        val nombre = preferences.getString(credencialesNames.NOMBRE, credencialesNames.CAMPO_VACIO)

        return nombre
    }

    fun guardarCredenciales(u : Usuario)
    {
        val editor = preferences.edit()

        editor.putInt(credencialesNames.ID_USUARIO,u.idUsuario)
        editor.putString(credencialesNames.NOMBRE,u.nombre)
        editor.putString(credencialesNames.PASS,u.pass)
        editor.putString(credencialesNames.CORREO,u.correo)
        editor.putString(credencialesNames.DNI, u.dni)
        editor.putString(credencialesNames.FECHA_NAC,u.fechaNac)
        editor.putString(credencialesNames.SEXO.toString(),u.sexo.toString())
        editor.putBoolean(credencialesNames.STATUS,u.status)
        editor.apply()
    }
}