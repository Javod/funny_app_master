package com.example.funny.com.proyecto.com.proyecto.adaptadores

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.TextView
import com.example.funny.R
import com.example.funny.com.proyecto.beans.Usuario

class ModeloUsuariosEnLista(var lista : List<Usuario>, var context: Context) : BaseAdapter()
{
    override fun getView(p0: Int, p1: View?, p2: ViewGroup?): View {
        var view= LayoutInflater.from(context).inflate(R.layout.modelo_item_usuario_en_lista,p2, false)



        val holder = BindViewholder(view)
        holder.setDataBind(lista.get(p0))
        holder.setNomOrden(p0+1)
        return view
    }

    override fun getItem(p0: Int): Any {
        return lista.get(p0)
    }

    override fun getItemId(p0: Int): Long {
        return p0.toLong()
    }

    override fun getCount(): Int {
        return lista.size
    }


    private  class BindViewholder(view : View)
    {
        private lateinit var nombre : TextView
        private lateinit var dni : TextView
        private lateinit var numOrden : TextView

        init {
            nombre = view.findViewById(R.id.nombre_usuario_en_lista)
            dni = view.findViewById(R.id.dni_usuario_en_lista)
            numOrden = view.findViewById(R.id.num_orden)
        }
        fun setDataBind(user : Usuario)
        {
            nombre.text=user.nombre;
            dni.text="DNI: ${user.dni}"
        }

        fun setNomOrden(num : Int)
        {
            numOrden.text="${num}."
        }
    }

}