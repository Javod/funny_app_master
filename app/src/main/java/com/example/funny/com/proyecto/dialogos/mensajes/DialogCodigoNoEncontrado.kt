package com.example.funny.com.proyecto.dialogos.mensajes

import android.app.AlertDialog
import android.app.Dialog
import android.os.Bundle
import android.view.View
import android.widget.Button
import androidx.fragment.app.DialogFragment
import com.example.funny.R

class DialogCodigoNoEncontrado(val listener : InteractionDialogCodigoNoEncontrado) : DialogFragment()
{

    lateinit var  btnAceptarCodigoNoEncontrado : Button

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {

        var view = activity!!.layoutInflater.inflate(R.layout.dialog_codigo_no_encontrado, null)

        btnAceptarCodigoNoEncontrado = view.findViewById(R.id.btn_aceptar_codigo_no_encontrado)

        btnAceptarCodigoNoEncontrado.setOnClickListener(View.OnClickListener {
            listener!!.cerraDialogCodigoNoEncontrado()
        })

        var dialog = AlertDialog.Builder(activity)

        dialog.setView(view)

        return dialog.create()
    }


    interface InteractionDialogCodigoNoEncontrado
    {
        fun cerraDialogCodigoNoEncontrado()
    }
}