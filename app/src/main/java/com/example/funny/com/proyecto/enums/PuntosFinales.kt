package com.example.funny.com.proyecto.enums

public class PuntosFinales {

    companion object{
       const  val urlRaiz = "https://frozen-fortress-60394.herokuapp.com/rest.v1.servicio.funny/"

        //Puntos finales para club
        //GET
        //clubes/{idLocalidad}
        const val clubesByIdLocalidad = "clubes/"


        const val allClubes = "all.clubes/"

        //club  por id de club
        const val clubByIdClub="club.by.id/"

        //Puntos finales para eventos
        //GET



        //eventos.get/{idEvento}
        const val eventoByIdEvento = "eventos.get/"

        const val allEventos="all.eventos/"

        //eventos.get.idLocal/{idLocal}
        const  val eventosByIdLocal = "eventos.get.idLocal/"

        const val eventosByIntegrante= "eventos.by.integrante/"


        //Puntos finales para Integrante lista

        //integrantes.by.idlista/{idLista}
        const val integrateByIdLista = "integrantes.by.idlista/"

        //integrante.by.id/{idIntegrante}
        const  val integranteByIdIntegrante = "integrante.by.id/"

        //POST
        const val postIntegrante = "integrante.post/"

        //Puntos finales para la LIsta

        //GET
        //lista/{idLista}
        const val listaById = "lista/"

        const val listaByCodLista ="lista.by.codLista/"


        const val listaPost = "lista.post/"



        //Puntos finales para Localidad
        //GET
        const val allLocalidades = "all.localidades/"

        //localidad/{idLocalidad}
        const val localidadById = "localidad/"

        //Puntos finales para Promocion

        //promocion/{idPromocion}
        const val promocionById = "promocion/"
        //promociones/{idlocal}
        const val promocionesByIdClub="promociones/"

        //Puntos finales para usuario

        const val usuarioPost="usuario.post/"
        const val usuarioPut="usuario.put/"
        const val usuaiosEnLista = "usuarios.en.lista/"

        const val validarLoginUser="validar.login.user/"
    }


}