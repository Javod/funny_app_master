package com.example.funny

import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.provider.CalendarContract
import android.widget.ListView
import com.example.funny.com.proyecto.beans.Evento
import com.example.funny.com.proyecto.beans.Usuario
import com.example.funny.com.proyecto.beans.com.proyecto.beans.listas.Lista
import com.example.funny.com.proyecto.com.proyecto.adaptadores.ModeloUsuariosEnLista
import com.example.funny.com.proyecto.enums.PuntosFinales
import com.example.funny.com.proyecto.retrofitconnetion.ServiceEndPoint
import com.example.funny.com.proyecto.utils.ConversorFormatoFecha
import com.squareup.picasso.Picasso
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_mis_integrantes_en_lista.*
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.*

class MisIntegrantesEnLista : AppCompatActivity() {

    lateinit var lista : Lista
    lateinit var evento : Evento
    lateinit var listaDeIntegrantes : List<Usuario>

    private var myCompositeDisposable: CompositeDisposable? = null

    private var myCompositeDisposable2: CompositeDisposable? = null

    lateinit var listViewIntegrantes : ListView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_mis_integrantes_en_lista)

        initCamponents()

        val bundle : Bundle = intent.extras

        if(bundle!=null)
        {
            lista = bundle.getSerializable("lista") as Lista

            if(lista!=null)
            {
                getDataEventoRest(lista.idEvento)
                getDataUsuariosRest(lista.idLista)

                refrescar_integrantes.setOnRefreshListener {
                    getDataUsuariosRest(lista.idLista)
                    refrescar_integrantes.isRefreshing=false
                }
            }
        }

    }

    private fun initCamponents()
    {
        myCompositeDisposable = CompositeDisposable()
        myCompositeDisposable2= CompositeDisposable()
        listViewIntegrantes = findViewById(R.id.lista_integrantes_de_mi_lista)
    }


    private fun getDataUsuariosRest(idLista : Int)
    {
        val retrofit = Retrofit.Builder().baseUrl(PuntosFinales.urlRaiz).addConverterFactory(
            GsonConverterFactory.create()).addCallAdapterFactory(RxJava2CallAdapterFactory.create()).build();

        val restCliente = retrofit.create(ServiceEndPoint::class.java)
        myCompositeDisposable2!!.add(restCliente.getUsuariosEnLista(idLista)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe(this::asignarUsuarios))
    }

    private fun asignarUsuarios(listaUsers : List<Usuario>)
    {
        this.listaDeIntegrantes = listaUsers
        if(this.listViewIntegrantes!=null)
        {
            var adapter = ModeloUsuariosEnLista(this.listaDeIntegrantes,this)
            listViewIntegrantes.adapter=adapter
            cantidadIntegrentes(listaUsers.size)
        }
    }

    private fun getDataEventoRest(idEvento : Int)
    {
        val retrofit = Retrofit.Builder().baseUrl(PuntosFinales.urlRaiz).addConverterFactory(
            GsonConverterFactory.create()).addCallAdapterFactory(RxJava2CallAdapterFactory.create()).build();

        val restCliente = retrofit.create(ServiceEndPoint::class.java)

        myCompositeDisposable2!!.add(restCliente.eventoByIdEvento(idEvento)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe(this::asignarEvento))
    }

    private fun asignarEvento(evento : Evento)
    {
        var conversor = ConversorFormatoFecha(evento.fecha)
        this.evento = evento

        if(evento.fotoEvento.isEmpty())
        {
            Picasso.get().load("https://pbs.twimg.com/profile_images/1062007558418907137/xZtVMnGz_400x400.jpg").into(imagen_evento_i)
        }
        else
        {
            Picasso.get().load(evento.fotoEvento).into(imagen_evento_i)
        }

        dia_evento_i.text="${conversor.diaEvento()}"
        mes_evento_i.text=conversor.nombreMes()
        nombre_evento_i.text = evento.nombreEvento
        nombre_club_i.text=evento.nombreClub
        precio_i.text="Precio: S/. ${evento.precioEntrada}"


        verificarEstadoEvento(evento.fecha)
        fechaVencimientoLista(evento.fecha)
    }


    private fun cantidadIntegrentes(cantidad : Int)
    {
        num_inte.text="En Lista: ${cantidad}"
    }

    private fun enviarEstado(estado : Boolean)
    {
        if(estado)
        {
            estado_lista.text="Activa"
            estado_lista.setTextColor(Color.parseColor("#67CF41"))
        }
        else
        {
            estado_lista.text="Caducada"
            estado_lista.setTextColor(Color.RED)
        }
    }

    fun verificarEstadoEvento(fecha : String)
    {
        var fechaArray = fecha.split("-")

        var anio = Integer.valueOf(fechaArray[0])
        var mes =  Integer.valueOf(fechaArray[1])
        var dia = Integer.valueOf(fechaArray[2])

        val c= Calendar.getInstance()
        val y = c.get(Calendar.YEAR)
        val m = c.get(Calendar.MONTH)+1
        val d = c.get(Calendar.DAY_OF_MONTH)

        if(anio>=y)
        {
            if(mes>m)
            {
                enviarEstado(true)
            }
            else if (mes==m)
            {
                if(dia>d) {
                    enviarEstado(true)
                }
                else {
                    enviarEstado(false)
                }
            }
            else{
                enviarEstado(false)
            }
        }
        else {
            enviarEstado(false)
        }


    }

    private fun fechaVencimientoLista(fecha : String)
    {
        fecha_vencimiento.text="Vence el: ${fecha}"
    }

}

