package com.example.funny

import android.app.DatePickerDialog
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.*
import com.example.funny.com.proyecto.beans.Usuario
import com.example.funny.com.proyecto.credenciales.Credenciales
import com.example.funny.com.proyecto.enums.PuntosFinales
import com.example.funny.com.proyecto.retrofitconnetion.ServiceEndPoint
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import com.example.funny.com.proyecto.utils.Response
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.*

class EditarDatosUsuario : AppCompatActivity() {

    lateinit var nombre : EditText
    lateinit var correo : EditText
    lateinit var dni : EditText
    lateinit var pass : EditText
    lateinit var opSexo : RadioGroup
    lateinit var fechaNac : TextView
    lateinit var btnActualizar : Button
    var sexo="S"
    var idUser=0

    private var myCompositeDisposable: CompositeDisposable? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_editar_datos_usuario)
        initComponents()
        recuperarDatosDeUsuario()


        fechaNac.setOnClickListener{obtenerFechaNac()}

        btnActualizar.setOnClickListener(View.OnClickListener{

            if (nombre.text.isEmpty()) {
                mensaje("Ingrese su nombre por favor")
            } else if (correo.text.isEmpty()) {
                mensaje("Ingrese su correo por favor")
            } else if (pass.text.isEmpty()) {
                mensaje("Ingrese una contaseña por favor")
            }
            if (fechaNac.text.isEmpty()) {
                mensaje("Ingrese el año de su nacimiento por favor")

            } else {

                //Validaciones de opccion de sexo
                val id = opSexo.checkedRadioButtonId

                if (id == R.id.option_hombre_e) {
                    sexo = "H"
                } else if (id == R.id.option_mujer_e) {
                    sexo = "M"
                } else if (id == R.id.option_otro_e) {
                    sexo = "O"
                }

                //Objeto usuario para guardar

                var usuario = Usuario(
                    idUser,
                    nombre.text.toString(),
                    pass.text.toString(),
                    correo.text.toString(),
                    dni.text.toString(),
                    fechaNac.text.toString(),
                    sexo,
                    true
                )
                verificarDatosusuarioRest(usuario)
            }

        })
    }


    fun initComponents()
    {
        nombre = findViewById(R.id.txt_nombre_user_e)
        correo = findViewById(R.id.txt_correo_user_e)
        dni = findViewById(R.id.txt_dni_user_e)
        pass = findViewById(R.id.txt_pass_user_e)
        opSexo = findViewById(R.id.options_sexo_e)
        fechaNac = findViewById(R.id.txt_fecha_nac_e)
        btnActualizar = findViewById(R.id.btn_editar_datos_user)
        myCompositeDisposable = CompositeDisposable()
    }

    fun verificarDatosusuarioRest(user : Usuario)
    {
        val retrofit = Retrofit.Builder().baseUrl(PuntosFinales.urlRaiz).addConverterFactory(
            GsonConverterFactory.create()).addCallAdapterFactory(RxJava2CallAdapterFactory.create()).build();

        val restCliente = retrofit.create(ServiceEndPoint::class.java)

        myCompositeDisposable!!.add(restCliente.putDatosUsuario(user)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe(this::setDatauser))

    }

    fun setDatauser(response : Response)
    {
        if(!response.succes)
        {
            mensaje("Error, Intentelo mas tarde por favor")
        }
        else
        {
            var u =  response.data as Usuario

            guardarDatos(u)

            mensaje("Sus datos se actualizaron con éxito")

            startActivity(Intent(this,UserActivity::class.java))
            finish()
        }

    }



    fun recuperarDatosDeUsuario()
    {
        var credenciales = Credenciales(this)
        var user = credenciales.getDataUser() as Usuario

        idUser=user.idUsuario
        nombre.setText(user.nombre)
        correo.setText(user.correo)
        dni.setText(user.dni)
        fechaNac.setText(user.fechaNac)

        val id = opSexo.checkedRadioButtonId

        if(user.sexo=="H")
        {
            opSexo.check(R.id.option_hombre_e)
        }else if(user.sexo=="M")
        {
            opSexo.check(R.id.option_mujer_e)
        }
        else if(user.sexo=="O")
        {
            opSexo.check(R.id.option_otro_e)
        }

    }

    fun obtenerFechaNac(){

        val c= Calendar.getInstance()
        val y = c.get(Calendar.YEAR)
        val m = c.get(Calendar.MONTH)
        val d = c.get(Calendar.DAY_OF_MONTH)
        var fech : String
        val dialogFech = DatePickerDialog(this, DatePickerDialog.OnDateSetListener{ _, y, m, d->

            var dia="${d}"
            var mes = "${m+1}"
            if(d<10 )
            {
                dia="0${d}"
            }
            if(m<9)
            {
                mes = "0${m+1}"
            }

            fech = "${y}-${mes}-${dia}"
            fechaNac.text=fech


        },y,m,d)

        dialogFech.show()

    }

    private fun guardarDatos(user : Usuario)
    {
        val credenciales = Credenciales(this)
        credenciales.guardarCredenciales(user)
    }

    fun mensaje(msj :String)
    {
        Toast.makeText(this,msj, Toast.LENGTH_LONG).show()
    }
}
