package com.example.funny

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.View
import android.widget.ImageView
import android.widget.Toast

import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProviders
import androidx.viewpager.widget.ViewPager
import com.example.funny.com.proyecto.beans.Club
import com.example.funny.com.proyecto.beans.Evento
import com.example.funny.com.proyecto.beans.com.proyecto.beans.listas.Lista
import com.example.funny.com.proyecto.com.proyecto.local.vista.tab.ViewPagerAdapterLocal
import com.example.funny.com.proyecto.credenciales.Credenciales
import com.example.funny.com.proyecto.dialogos.mensajes.DialogComfirmation
import com.example.funny.com.proyecto.dialogos.mensajes.DialogCrearLista
import com.example.funny.com.proyecto.enums.ListaLocales
import com.example.funny.com.proyecto.enums.PuntosFinales
import com.example.funny.com.proyecto.retrofitconnetion.ServiceEndPoint
import com.example.funny.com.proyecto.vistas.fragmentos.FragmentoLocalEventos
import com.example.funny.com.proyecto.vistas.fragmentos.FragmentoLocalPromociones
import com.example.funny.com.viewmodel.ListaViewModel
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.android.material.tabs.TabLayout
import com.squareup.picasso.Picasso
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_local.*
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.*
import kotlin.collections.ArrayList

class LocalActivity : AppCompatActivity(), FragmentoLocalEventos.OnFragmentInteractionListener, FragmentoLocalPromociones.OnFragmentInteractionListener,
        DialogCrearLista.InteractionDialog, DialogComfirmation.InteractionCerrarDialog
{

    private var locales : ArrayList<Club>  = ArrayList()
    private var imgLocal : ImageView? = null


    private var tabLayout : TabLayout? = null
    private var pager : ViewPager? = null
    private var pagerLocal : ViewPagerAdapterLocal? = null


    var dialog : DialogCrearLista? = null
    var dialogo : DialogComfirmation? = null

    lateinit var evento : Evento
    lateinit var local : Club
    lateinit var lista  : Lista

    lateinit var listaViewModel: ListaViewModel

    lateinit var fragmentoLocalEventos: FragmentoLocalEventos
    lateinit var fragmentoClubPromos : FragmentoLocalPromociones
    lateinit var enviarIdClub : Bundle
    lateinit var enviarIdClub2 : Bundle

    var direccion  = "Lima-Peru"


    var nombreEvento ="nombre_evento"
    var idClub : Int = 0

    var bandera  = false

    private var myCompositeDisposable: CompositeDisposable? = null

    private var myCompositeDisposable2: CompositeDisposable? = null
    lateinit var credenciales : Credenciales
    lateinit var  fab : FloatingActionButton

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_local)
        setSupportActionBar(toolbar)

        listaViewModel = run{ViewModelProviders.of(this).get(ListaViewModel::class.java)}

        credenciales  = Credenciales(this)

        initcomponents()



        val bundle : Bundle = intent.extras


        if(bundle!=null)
        {
            if(bundle.getSerializable("evento")!=null)
            {
                evento = bundle.getSerializable("evento") as Evento
                setTitle(evento.nombreClub)

                idClub = evento.idLocal
                getClubRest(evento.idLocal)

                bandera=true

            }else if(bundle.getSerializable("local")!=null)
            {
                local = bundle.getSerializable("local") as Club
                idClub = local.idAdministrador
                setTitle(local.nombreClub)
                //imgLocal?.setImageResource(local?.fotoLocal)
                Picasso.get().load(local.fotoClub).into(imgLocal)
                direccion=local.direccion
                bandera=false
            }

        }

        fab.setOnClickListener(View.OnClickListener {

            abrirMapaDireccionClub(direccion)
        })

        setUpView()
        setUpViewPager()

    }

    fun setUpView()
    {
        tabLayout = findViewById(R.id.tab_vistas_local)
        pager = findViewById(R.id.paginas)
        pagerLocal = ViewPagerAdapterLocal(getSupportFragmentManager())
    }

    fun setUpViewPager()
    {

        if(bandera)
        {
            enviarIdClub.putSerializable("evento_click",evento)
            enviarIdClub2.putInt("idClub",evento.idLocal)
        }
        else{
            enviarIdClub.putSerializable("id_club",idClub)
            enviarIdClub2.putInt("idClub",idClub)
        }
        fragmentoLocalEventos.arguments=enviarIdClub
        fragmentoClubPromos.arguments=enviarIdClub2
        pagerLocal?.addFragment(fragmentoLocalEventos,"Eventos")
        pagerLocal?.addFragment(fragmentoClubPromos,"Promociones")
        pager?.adapter=pagerLocal
        tabLayout?.setupWithViewPager(pager)

    }

    fun initcomponents()
    {
        fab = findViewById(R.id.fab)
        myCompositeDisposable = CompositeDisposable()
        myCompositeDisposable2 = CompositeDisposable()
        imgLocal = findViewById(R.id.img_local__vista)
        locales = ListaLocales().addLocales()!!
        dialog = DialogCrearLista(this)
        enviarIdClub = Bundle()
        enviarIdClub2= Bundle()
        fragmentoLocalEventos = FragmentoLocalEventos()
        fragmentoClubPromos = FragmentoLocalPromociones()
    }

    fun mostrarDialogCreatlista(nombreEvento : String)
    {
        val manager = supportFragmentManager
        dialog!!.nombreEvento=nombreEvento
        dialog!!.show(manager,"Dialog_crear_lista")
    }

    fun mostrarDialogComfirmatioLista(codigo : String)
    {
        val manager = supportFragmentManager
        dialogo = DialogComfirmation(codigo, this)
        dialogo!!.show(manager,"Dialog_Comfirmation")
    }


    fun mensaje(msj :String)
    {
        Toast.makeText(this,msj, Toast.LENGTH_SHORT).show()
    }

    fun getClubRest(idClub : Int)
    {
        val retrofit = Retrofit.Builder().baseUrl(PuntosFinales.urlRaiz).addConverterFactory(
            GsonConverterFactory.create()).addCallAdapterFactory(RxJava2CallAdapterFactory.create()).build();

        val restCliente = retrofit.create(ServiceEndPoint::class.java)

        myCompositeDisposable!!.add(restCliente.clubByIdClub(idClub)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe(this::dataClubRest))
    }

    fun dataClubRest( c : Club)
    {
        var club = c
        Picasso.get().load(c.fotoClub).into(imgLocal)
        direccion=c.direccion

    }

    fun listaPost(l : Lista)
    {
        val retrofit = Retrofit.Builder().baseUrl(PuntosFinales.urlRaiz).addConverterFactory(
            GsonConverterFactory.create()).addCallAdapterFactory(RxJava2CallAdapterFactory.create()).build();

        val restCliente = retrofit.create(ServiceEndPoint::class.java)

        myCompositeDisposable2!!.add(restCliente.postLista(l)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe(this::asignarLista))
    }

    fun asignarLista(l : Lista)
    {
        this.lista = l
        mostrarDialogComfirmatioLista(lista.codLista)
        guardarListaEvento(lista)
        dialog!!.dismiss()
    }


    //interfaz con metodo sobreescritopara recibir el nombre del evento
    override fun enviarNombrEvento(nombre: String, idEvento: Int) {
        nombreEvento = nombre
        this.idClub = idEvento
        mostrarDialogCreatlista(nombre)
    }
    //Interfaces para manejar los datos entre los fragmentos y la actividad

    override fun onFragmentInteraction(uri: Uri) {

    }

    //interfac del DialogCrearlista para comunicarse con la actividad padre
    override fun enviarAccion(action: Boolean) {

        var bandera=action
        if(bandera)
        {

                var listaTemp = Lista(0,idClub,credenciales.getDataUserId(),nombreEvento,"",obtenerFechaActual(),true)

                listaPost(listaTemp)
        }

    }

    fun guardarListaEvento(lista: Lista)
    {

        listaViewModel.save(lista)

    }

    fun abrirMapaDireccionClub(direc : String )
    {
        var parseDirec = direc.replace(" ","+")

        //mensaje(parseDirec)
        val intent : Intent = Uri.parse("geo:0,0?q=${parseDirec}").let {
            location-> Intent(Intent.ACTION_VIEW,location)
        }

        startActivity(intent)
    }

    fun obtenerFechaActual() : String{
        val c= Calendar.getInstance()
        val y = c.get(Calendar.YEAR)
        val m = c.get(Calendar.MONTH)+1
        val d = c.get(Calendar.DAY_OF_MONTH)

        return "${y}-${m}-${d}"
    }

    override fun cerrarDialog(dato: String) {
        dialogo!!.dismiss()
    }

}
