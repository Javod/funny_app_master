package com.example.funny

import android.app.DatePickerDialog
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.*
import com.example.funny.com.proyecto.beans.Usuario
import com.example.funny.com.proyecto.credenciales.Credenciales
import com.example.funny.com.proyecto.enums.PuntosFinales
import com.example.funny.com.proyecto.retrofitconnetion.ServiceEndPoint
import com.example.funny.com.proyecto.utils.Response
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.*

class Registrarse : AppCompatActivity() {

    private lateinit var txtNombre : EditText
    private lateinit var txtCorreo : EditText
    private lateinit var txtDni  : EditText
    private lateinit var txtPass : EditText
    private lateinit var optionSexo : RadioGroup
    private lateinit var txtFechaNac : TextView
    private lateinit var btnGuardar : Button
    private var sexo = "S"

    private var myCompositeDisposable: CompositeDisposable? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_registrarse)
        initComponents()



        txtFechaNac.setOnClickListener {obtenerFechaNac()}
        // Evento de guardar usuario
        btnGuardar.setOnClickListener(View.OnClickListener {


            //Validaciones de campos
            if(txtNombre.text.isEmpty())
            {
                mensaje("Ingrese su nombre por favor")
            }
            else if( txtCorreo.text.isEmpty())
            {
                mensaje("Ingrese su correo por favor")
            }
            else if( txtPass.text.isEmpty())
            {
                mensaje("Ingrese una contaseña por favor")
            }
            if(txtFechaNac.text.isEmpty())
            {
                mensaje("Ingrese el año de su nacimiento por favor")

            }

            else
            {

                //Validaciones de opccion de sexo
                val id = optionSexo.checkedRadioButtonId

                if(id==R.id.option_hombre)
                {
                    sexo="H"
                }else if(id==R.id.option_mujer)
                {
                    sexo="M"
                }
                else if(id==R.id.option_otro)
                {
                    sexo="O"
                }

                //Objeto usuario para guardar


                var usuario = Usuario(0,txtNombre.text.toString(),txtPass.text.toString(),txtCorreo.text.toString(),txtDni.text.toString(),txtFechaNac.text.toString(),sexo,true)

                verificarRegistroDeUsuarioRest(usuario)

                //Registro exitoso, mandar a la
                //actividad principal


            }

        })
    }


    //funcion para inicializar componentes
    private fun initComponents()
    {
        txtNombre = findViewById(R.id.txt_nombre_user)
        txtCorreo= findViewById(R.id.txt_correo_user)
        txtDni = findViewById(R.id.txt_dni_user)
        txtPass = findViewById(R.id.txt_pass_user)
        optionSexo = findViewById(R.id.options_sexo)
        txtFechaNac = findViewById(R.id.txt_fecha_nac)
        btnGuardar = findViewById(R.id.btn_guardar_datos_user)
        myCompositeDisposable = CompositeDisposable()

    }

    fun verificarRegistroDeUsuarioRest(user : Usuario)
    {
        val retrofit = Retrofit.Builder().baseUrl(PuntosFinales.urlRaiz).addConverterFactory(
            GsonConverterFactory.create()).addCallAdapterFactory(RxJava2CallAdapterFactory.create()).build();

        val restCliente = retrofit.create(ServiceEndPoint::class.java)

        myCompositeDisposable!!.add(restCliente.postRegistrarUsuario(user)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe(this::asignarUsuario))

    }

    fun asignarUsuario(response: Response)
    {

        if(!response.succes)
        {
            mensaje(response.message)
        }
        else
        {
            var u =  response.data as Usuario

            guardarDatos(u)

            mensaje(response.message)

            startActivity(Intent(this,MainActivity::class.java))
            finish()
        }

    }

    //funcion para guardar los datos del usuario
    private fun guardarDatos(user : Usuario)
    {
        val credenciales = Credenciales(this)

        credenciales.guardarCredenciales(user)
    }

    fun obtenerFechaNac(){

        val c= Calendar.getInstance()
        val y = c.get(Calendar.YEAR)
        val m = c.get(Calendar.MONTH)
        val d = c.get(Calendar.DAY_OF_MONTH)
        var fech : String
        val dialogFech = DatePickerDialog(this,DatePickerDialog.OnDateSetListener{ _, y, m, d->

            var dia="${d}"
            var mes = "${m+1}"
            if(d<10 )
            {
               dia="0${d}"
            }
            if(m<9)
            {
                mes = "0${m+1}"
            }

            fech = "${y}-${mes}-${dia}"
            txtFechaNac.text=fech


        },y,m,d)

        dialogFech.show()

    }

    fun mensaje(msj :String)
    {
        Toast.makeText(this,msj, Toast.LENGTH_LONG).show()
    }

}
